﻿using UnityEngine;
using System.Collections;

public class WC_Info : MonoBehaviour
{
	public static string[,] info = new string[,] { {
			"Evaporation",
			"The sun, which drives the water cycle, heats water in oceans and seas. Water evaporates as water vapor into the air. Ice and snow can sublimate directly into water vapour. Due to the significant difference in molecular mass, water vapor in gas form gain height in open air as a result of buoyancy and start aggregating."
		}, {
			"Condensation",
			"The lowered temperature causes water vapour to condense into a tiny liquid water droplet which is heavier than the air, such that it falls unless supported by an updraft. A huge concentration of these droplets over a large space up in the atmosphere become visible as cloud."
		}, {
			"Precipitation",
			"Air currents move water vapour around the globe, cloud particles collide, combine and become heavier leading to fall out of the upper atmospheric layers due to gravity this process is called precipitation. Some precipitation falls as snow or hail, sleet, and can accumulate as ice caps and glaciers, which can store frozen water for thousands of years."
		}, {
			"Collection",
			"In river valleys and floodplains there is often continuous water exchange between surface water and groundwater in the hyporheic zone. Over time, the water returns to the ocean, to continue the water cycle."
		}
	};

}
