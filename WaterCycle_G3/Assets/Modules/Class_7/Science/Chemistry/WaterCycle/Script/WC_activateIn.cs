﻿using UnityEngine;
using System.Collections;

public class WC_activateIn : MonoBehaviour {
	public GameObject[] objects;
	public float[] timer;
	bool isInitialised;
	int currentObject,timeCount;
	// Use this for initialization
	void Update () {
		if (WaterCycleController.isTargetFound && !isInitialised && timeCount == 0) {
			Invoke ("startme", timer[timeCount]);
			isInitialised = true;
		}else if(!WaterCycleController.isTargetFound){
			isInitialised = false;
			timeCount = 0;
			if(objects [1].name=="ArrowsAndLabel")objects [1].GetComponent<WC_ArrowGenerator> ().StopAll ();
			for (int i = 0; i < objects.Length; i++)
				objects [i].SetActive (false);
		}
	}

	void startme(){
		if (timeCount < objects.Length && WaterCycleController.isTargetFound && !this.IsInvoking("startme")) {
			objects [timeCount].SetActive (true);
			if (timeCount < objects.Length-1)
				timeCount = timeCount + 1;
			Invoke ("startme", timer[timeCount]);
		}
	}
}
