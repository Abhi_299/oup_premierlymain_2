﻿using UnityEngine;
using System.Collections;

public class WC_RotateOnAxis : MonoBehaviour {
	public float rotateSpeed;

	// Update is called once per frame
	void Update () {
		transform.RotateAround (this.transform.position, Vector3.forward, rotateSpeed);
	}
}
