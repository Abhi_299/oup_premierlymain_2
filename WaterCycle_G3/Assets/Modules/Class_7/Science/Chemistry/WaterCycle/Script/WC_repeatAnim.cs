﻿using UnityEngine;
using System.Collections;

public class WC_repeatAnim : MonoBehaviour {
	public int maxStay;
	public string stateName;
	void Start(){
		Invoke ("repeat",maxStay);
	}

	void repeat(){
		this.GetComponent<Animator> ().Play (stateName, -1, 0f);
		if(WaterCycleController.isTargetFound)Invoke ("repeat",maxStay);
	}
}
