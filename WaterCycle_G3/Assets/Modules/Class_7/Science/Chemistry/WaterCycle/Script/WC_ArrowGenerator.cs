﻿using UnityEngine;
using System.Collections;

public class WC_ArrowGenerator : MonoBehaviour
{
	public GameObject ArrowPrefab;
	public int maxNoOfArrow;
	public float timeDiff;
	GameObject[] arrowObj;
	int currentArrow;
	bool isInitialised;

	void Start ()
	{
		arrowObj = new GameObject[maxNoOfArrow];
		for (int i = 0; i < maxNoOfArrow; i++) {
			Object temp = Instantiate (ArrowPrefab, new Vector3 (0, 0, 0), Quaternion.identity);
			temp.name = "Arrow";
			arrowObj [i] = GameObject.Find ("Arrow").gameObject;
			arrowObj [i].SetActive (false);
			arrowObj [i].transform.parent = this.gameObject.transform;
			arrowObj [i].transform.localScale = new Vector3 (0.2f, 0.2f, 0.2f);
		}
	}

	void Update ()
	{
		if (WaterCycleController.isTargetFound && !isInitialised) {
			Invoke ("release", timeDiff);
			isInitialised = true;
		}
	}

	public void StopAll ()
	{
		if (!WaterCycleController.isTargetFound && isInitialised) {
			currentArrow = 0;
			isInitialised = false;
			for (int i = 0; i < maxNoOfArrow; i++) {
				arrowObj [i].SetActive (false);
				arrowObj [i].transform.parent = this.gameObject.transform;
				arrowObj [i].GetComponent<Animator> ().Play ("Arrow", -1, 0f);
				//arrowObj [i].GetComponent<Animator> ().Stop ();
			}
		}
	}

	void release ()
	{
		if (WaterCycleController.isTargetFound) {
			arrowObj [currentArrow].SetActive (true);
			arrowObj [currentArrow].GetComponent<Animator> ().Play ("Arrow");
			if (currentArrow < maxNoOfArrow - 1) {
				currentArrow = currentArrow + 1;
				Invoke ("release", timeDiff);
			}
		}
	}
}
