﻿using UnityEngine;
using System.Collections;

public class WC_CloudGenerator : MonoBehaviour {
	public GameObject CloudPrefab;
	public int maxNoOfClouds;
	public float timeDiff; 
	GameObject[] cloudObj;
	int currentFloatingCloud;

	// Use this for initialization
	void Start () {
		cloudObj = new GameObject[maxNoOfClouds];
		for (int i = 0; i < maxNoOfClouds; i++) {
			Object temp = Instantiate (CloudPrefab, new Vector3 (0, 0, 0), Quaternion.identity);
			temp.name = "CloudParent";
			cloudObj [i] = GameObject.Find ("CloudParent").gameObject;
			cloudObj [i].SetActive (false);
			cloudObj [i].transform.parent = this.gameObject.transform;
		}
		//Invoke ("changeAxis",3f);
		this.transform.localEulerAngles = new Vector4(this.transform.localEulerAngles.x,this.transform.localEulerAngles.y+90f,this.transform.localEulerAngles.z);
		Invoke ("release",timeDiff);
	}

	void release(){
		cloudObj [currentFloatingCloud].SetActive (true);
		if (currentFloatingCloud < maxNoOfClouds - 1) {
			currentFloatingCloud = currentFloatingCloud + 1;
			Invoke ("release",timeDiff);
		}
	}
}
