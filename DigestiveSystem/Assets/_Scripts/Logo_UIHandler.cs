﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logo_UIHandler : MonoBehaviour {

	// canvas control gameobject.
	[Header("Canvas gameObjects")]
	public GameObject  Canvas,  wizarLogo;

	// screenshot handler objects.
	[Header("screenshot handler")]
	public GameObject screenShotSavedPopUp;
	public GameObject screenShotPanel;
	public GameObject screenShotButton;
	public GameObject screenShot_ui;

	[Header("Camera")]
	public Camera arCamera;

	private List<GameObject> EnabledCanvasObject;

	void Awake()
	{
		EnabledCanvasObject=new List<GameObject>();
	}



	void TurnOnUIAfterImageClick()
	{
		ScreenShotUIManger(true);
	}
	void ChangeCamera()
	{
		print("Camera changed....");
		Canvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		Canvas.GetComponent<Canvas>().worldCamera = arCamera;
	}

	// UI off when screenshot click and logo show
	public void ScreenShotUIManger(bool status)
	{
		
		if (!status)
		{
			Invoke("ChangeCamera", 3f);

			// to disable all the canvas;
			for (int i = 0; i < Canvas.transform.childCount; i++)
			{
				if (Canvas.transform.GetChild (i).gameObject.activeSelf) {
					EnabledCanvasObject.Add (Canvas.transform.GetChild (i).gameObject);
				}
				Canvas.transform.GetChild (i).gameObject.SetActive (false);
			}

			screenShot_ui.SetActive (true);
		}
		else
		{
			Canvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
		}

		print(" status "+ status + " "+ Canvas.GetComponent<Canvas>().renderMode);


		GameObjectOnOff(wizarLogo, !status);
		CancelInvoke("TurnOnUIAfterImageClick");
		Invoke("TurnOnUIAfterImageClick", 3.25f);
	}



	public void GameObjectOnOff(GameObject @object, bool status)
	{
		if (@object != null)
		{
			@object.SetActive(status);
		}
	}

	public void enableCanvas()
	{
		for (int i = 0; i < EnabledCanvasObject.Count; i++) 
		{
			EnabledCanvasObject [i].SetActive (true);
		}
		EnabledCanvasObject.Clear ();
	}

	//#endregion
}
