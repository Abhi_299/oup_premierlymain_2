﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButtonScript : MonoBehaviour
{
	public string bundleIdentifier;
	public static BackButtonScript instance;

	private void Start()
	{
		instance = this;
	}

	public void BackBtn()
	{
        OUPAnalytics.instance.SendSoapRequest();
        Debug.Log("Bundle : " + bundleIdentifier);
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
		AndroidJavaObject launchIntent = null;

		try
		{
			launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleIdentifier);
		}
		catch (System.Exception e)
		{
			Application.OpenURL("https://play.google.com/store/apps/details?id=com.excelSoft.oupi.myBag");
		}

		try
		{
			ca.Call("startActivity", launchIntent);
		}
		catch (System.Exception e)
		{
			Application.OpenURL("https://play.google.com/store/apps/details?id=com.excelSoft.oupi.myBag");
		}
		up.Dispose();
		ca.Dispose();
		packageManager.Dispose();
		launchIntent.Dispose();
	}
}
