﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvexAndModuleHandler : MonoBehaviour
{

	public GameObject[] imageTargets;
    public GameObject[] canvexUI;
    public GameObject[] imageTargetChild;
    public List<GameObject> loadedModel;

    //   void OnEnable ()
    //{
    //	DefaultTrackableEventHandler.onImageTargetDetected += DefaultTrackableEventHandler_onImageTargetDetected;
    //}


    //void Disable ()
    //{
    //	DefaultTrackableEventHandler.onImageTargetDetected -= DefaultTrackableEventHandler_onImageTargetDetected;
    //}


    private void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += TrackableEventHandler_OnImageTargetDetected;
    }

    private void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= TrackableEventHandler_OnImageTargetDetected;
    }


    void TrackableEventHandler_OnImageTargetDetected(string arg, Transform _transform, bool isFound, LoadModelOnDetection.SceneType sceneType)
	{
        if (isFound && _transform != null)
        {
            //loadedModel.Add(_transform)

            for (int i = 0; i < imageTargets.Length; i++)
            {
                Debug.Log("gameobject " + arg.ToString() +":"+ imageTargets[i].name);
                if (arg.ToLower().Equals(imageTargets[i].name.ToLower()))
                {
                    Debug.Log("canvex true.........");
                    if (canvexUI[i] != null)
                        canvexUI[i].SetActive(true);
                    if (imageTargetChild.Length > 0&&imageTargetChild[i] != null)
                        imageTargetChild[i].SetActive(true);
                }
                else
                {
                    Debug.Log("canvex false..........");
                    if (canvexUI[i] != null)
                        canvexUI[i].SetActive(false);
                    if (imageTargetChild.Length > 0&& imageTargetChild[i] != null)
                        imageTargetChild[i].SetActive(false);
                }
            }
        }
        else
        {
            for (int i = 0; i < imageTargets.Length; i++)
            {
                if(canvexUI[i]!=null)
                    canvexUI[i].SetActive(false);
                if (imageTargetChild.Length>0 && imageTargetChild[i] != null)
                    imageTargetChild[i].SetActive(false);

            }
         }
	}
}
