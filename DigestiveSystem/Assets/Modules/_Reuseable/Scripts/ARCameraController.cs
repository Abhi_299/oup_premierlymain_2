﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Vuforia;

public class ARCameraController : MonoBehaviour
{

	#region public variable

	public bool isARModule, isSkyboxOn;


	#endregion

	void Start ()
	{
		Debug.Log ("start called " + isARModule);
		if (isARModule)
			AR_ModulesActivate ();
		else
			ThreeD_ModulesActivate ();
	}

	void Update ()
	{

		if (isARModule && GetComponent <VuforiaBehaviour> ().enabled == false) {
			//VuforiaRuntime.Instance.InitVuforia ();
			if (isSkyboxOn)
				GetComponent <Camera> ().clearFlags = CameraClearFlags.Skybox;
			else
				GetComponent <Camera> ().clearFlags = CameraClearFlags.SolidColor;
			GetComponent <VuforiaBehaviour> ().enabled = true;
		}
	}

	public	void ThreeD_ModulesActivate ()
	{
		
		if (GetComponent <VuforiaBehaviour> () != null && GetComponent <VuforiaBehaviour> ().enabled == true) {
			//VuforiaRuntime.Instance.InitVuforia ();
			Debug.Log ("deactivate vuforia");
			if (isSkyboxOn)
				GetComponent <Camera> ().clearFlags = CameraClearFlags.Skybox;
			else
				GetComponent <Camera> ().clearFlags = CameraClearFlags.SolidColor;
			GetComponent <VuforiaBehaviour> ().enabled = false;
		}

	}

	public void AR_ModulesActivate ()
	{
		if (GetComponent <VuforiaBehaviour> ().enabled == false) {
			//VuforiaRuntime.Instance.InitVuforia ();
			if (isSkyboxOn)
				GetComponent <Camera> ().clearFlags = CameraClearFlags.Skybox;
			else
				GetComponent <Camera> ().clearFlags = CameraClearFlags.SolidColor;
			GetComponent <VuforiaBehaviour> ().enabled = true;
		}

	}
}
