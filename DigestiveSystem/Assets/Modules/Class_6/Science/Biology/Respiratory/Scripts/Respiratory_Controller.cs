﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Respiratory_Controller : MonoBehaviour
{

	public GameObject panelParent;
	public GameObject panelIntro;
	public GameObject panelHelp;
	public GameObject pnl_NoImageTarget;

	public GameObject panelInfo;
	public GameObject infoHeading;
	public GameObject infoDes;

	public GameObject noseIn;
	public GameObject noseOut;

	public GameObject panelLabels;

	public GameObject[] CO2;
	public GameObject[] O2;

	public AudioClip buttonClick;

	bool isInfoOpen;
	bool isHelpOpen;
	bool isIntroOpen;

	float timer;
	bool isInhaling;


	// Update is called once per frame
	void Update ()
	{
		timer = timer + Time.deltaTime;
		if (timer > 1) {
			isInhaling = !isInhaling;
			timer = 0;
		}

		if (isInhaling) {
			noseIn.SetActive (true);
			noseOut.SetActive (false);
			for (int i = 0; i < CO2.Length; i++) {
				CO2 [i].SetActive (true);
				O2 [i].SetActive (false);
			}
		} else {
			noseIn.SetActive (false);
			noseOut.SetActive (true);
			for (int i = 0; i < CO2.Length; i++) {
				CO2 [i].SetActive (false);
				O2 [i].SetActive (true);
			}
		}
	}

	public void btn_intro ()
	{
		isIntroOpen = !isIntroOpen;
		iTween.ScaleTo (panelHelp, new Vector3 (0, 0, 0), 0.5f);
		iTween.ScaleTo (panelInfo, new Vector3 (0, 0, 0), 0.5f);
		if (!isIntroOpen) {
			iTween.ScaleTo (panelIntro, new Vector3 (0, 0, 0), 0.5f);
			panelLabels.SetActive (true);
		} else {
			iTween.ScaleTo (panelIntro, new Vector3 (1, 1, 1), 0.5f);
			panelLabels.SetActive (false);
		}
		playbuttonClick ();
	}

	public void btn_help ()
	{
		isHelpOpen = !isHelpOpen;
		iTween.ScaleTo (panelIntro, new Vector3 (0, 0, 0), 0.5f);
		iTween.ScaleTo (panelInfo, new Vector3 (0, 0, 0), 0.5f);
		if (!isHelpOpen) {
			iTween.ScaleTo (panelHelp, new Vector3 (0, 0, 0), 0.5f);
			panelLabels.SetActive (true);
		} else {
			iTween.ScaleTo (panelHelp, new Vector3 (1, 1, 1), 0.5f);
			panelLabels.SetActive (false);
		}
		playbuttonClick ();
	}

	public void btn_info (string arg)
	{
		isInfoOpen = !isInfoOpen;
		if (!isIntroOpen && !isHelpOpen) {
			// Can ask many panel here only and set accordingly
			if (!isInfoOpen) {
				iTween.ScaleTo (panelInfo, new Vector3 (0, 0, 0), 0.5f);
				panelLabels.SetActive (true);
			} else {
				iTween.ScaleTo (panelInfo, new Vector3 (1, 1, 1), 0.5f);
				panelLabels.SetActive (false);
			}
		}
		if (arg != null) {
			showInfo (arg.Trim ());
		}
		playbuttonClick ();
	}


	void OnEnable ()
	{
		_ImageTargetDetected.onImageTargetDetected += onImageTargetDetected;
	}


	void OnDisable ()
	{
		_ImageTargetDetected.onImageTargetDetected -= onImageTargetDetected;
	}

	void onImageTargetDetected (GameObject @object, bool arg)
	{
		if (arg) {
			if (pnl_NoImageTarget != null) {
				iTween.ScaleTo (pnl_NoImageTarget, new Vector3 (0, 0, 0), 0.25f);
			}
//			if (panelParent != null)
//				panelParent.SetActive (true);
			if (panelLabels != null && !isIntroOpen && !isHelpOpen && !isInfoOpen)
				panelLabels.SetActive (true);
		}
		if (!arg) {
			if (pnl_NoImageTarget != null) {
				iTween.ScaleTo (pnl_NoImageTarget, new Vector3 (1, 1, 1), 0.25f);
			}
			if (panelParent != null)
				panelParent.SetActive (false);
			if (panelLabels != null)
				panelLabels.SetActive (false);
		}
	}

	void showInfo (string arg)
	{
		
		for (int i = 0; i < RespiratoryInfo.info.Length / 2; i++) {
			
			if (arg.Trim () == RespiratoryInfo.info [i, 0]) {
				infoHeading.GetComponent<Text> ().text = "" + RespiratoryInfo.heading [i];
				infoDes.GetComponent<Text> ().text = "" + RespiratoryInfo.info [i, 1];
			}
		}
		playbuttonClick ();
	}

	void playbuttonClick ()
	{
		GetComponent<AudioSource> ().PlayOneShot (buttonClick);
	}
}
