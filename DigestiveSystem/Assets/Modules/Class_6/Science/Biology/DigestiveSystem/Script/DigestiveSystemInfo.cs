﻿using UnityEngine;
using System.Collections;

public class DigestiveSystemInfo : MonoBehaviour {

	public static string[,] info = new string[,] { 
		{
			"Salivary_Glands", "There are three pairs of main salivary glands and between 800 and 1,000 minor salivary glands, all of which mainly serve the digestive process and also play an important role in the maintenance of dental health and general mouth lubrication, without which speech would be impossible."
		},
		{
			"Tongue", "Food enters the mouth where the first stage in the digestive process takes place, with the action of the tongue and the secretion of saliva. The tongue is a fleshy and muscular sensory organ, and the very first sensory information is received via the taste buds on its surface."
		},
		{
			"Oral_Cavity", "The oral cavity includes the lips, the inside lining of the lips and cheeks (buccal mucosa), the teeth, the gums, the front two-thirds of the tongue, the floor of the mouth below the tongue, and the bony roof of the mouth (hard palate)."
		},
		{
			"Liver", "The liver is the second largest organ (after the skin) and is an accessory digestive gland which plays a role in the body's metabolism. The liver has many functions some of which are important to digestion. The liver can detoxify various metabolites; synthesize proteins and produce biochemicals needed for digestion."
		},
		{
			"Stomach", "The stomach is a major organ of the gastrointestinal tract and digestive system. It a consistently J-shaped organ joined to the esophagus at its upper end and to the duodenum at its lower end.Gastric acid (informally gastric juice), produced in the stomach plays a vital role in the digestive process, it mainly contains hydrochloric acid and sodium chloride."
		},
		{
			"Gall_Bladder_Duodenum", "The gallbladder is a hollow part of the biliary system that sits just beneath the liver, with the gallbladder body resting in a small depression. It is a small organ where the bile produced by the liver is stored, before being released into the small intestine. Bile flows from the liver through the bile ducts and into the gallbladder for storage."
		},
		{
			"Pancreas", "The pancreas is a major organ functioning as an accessory digestive gland in the digestive system. It is both an endocrine gland and an exocrine gland.The endocrine part secretes insulin when the blood sugar becomes high; insulin moves glucose from the blood into the muscles and other tissues for use as energy."
		},
		{
			"Small_Intestine", "Food starts to arrive in the small intestine one hour after it is eaten, and after two hours the stomach has emptied. Until this time the food is termed a bolus. It then becomes the partially digested semi-liquid termed chyme. In the small intestine, the pH becomes crucial; it needs to be finely balanced in order to activate digestive enzymes."
		},
		{
			"Large_Intestine", "In the large intestine, the passage of the digesting food in the colon is a lot slower, taking from 12 to 50 hours until it is removed by defecation. The colon mainly serves as a site for the fermentation of digestible matter by the gut flora. The time taken varies considerably between individuals."
		},
		{
			"Rectum", "It’s the part of gastrointestinal tract, through which remaining semi-solid waste is removed by the coordinated contractions of the intestinal walls. Here, excreta reaches to exit."
		},
		{
			"Anus", "It’s the end part of gastrointestinal tract, Excretion happens from here."
		},
	};

	public static string[] heading = new string[] {
		"Salivary Glands",
		"Tongue",
		"Oral Cavity",
		"Liver",
		"Stomach",
		"Gall Bladder Duodenum",
		"Pancreas",
		"Small Intestine",
		"Large Intestine",
		"Rectum",
		"Anus"
	};
}
