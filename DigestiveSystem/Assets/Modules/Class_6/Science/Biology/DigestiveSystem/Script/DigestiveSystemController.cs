﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// While renaming the scipts refactor all the classes names with a prefix of ProjectName_
public class DigestiveSystemController : MonoBehaviour
{
    public static DigestiveSystemController instance;

    // parent of info , intro, help canvas to disable when image target is lost
    //public GameObject parentCanvas;

    //public GameObject pnl_intro;
    //public GameObject pnl_help;
    //public GameObject pnl_Info;

    //public GameObject infoDes;
    //public GameObject infoHeading;

    //// Panle when image target is lost
    //public GameObject pnl_NoImageTarget;

    //public GameObject Labels;

    //public AudioClip buttonClick;

    // current focus
    public enum WHAT_IS_SELECTED
	{
		HELP,
		INTRO,
		INFO,
		NO_IMAGE_TARGET}

	;

	// On/Off switch
	internal bool isInfoOpen;
    internal bool isHelpOpen;
    internal bool isIntroOpen;

    void Awake()
    {
        instance = this;    
    }

    // When i is clicked on the main screen to show what is app about
    public void opnIntro ()
	{
		playbuttonClick ();
		isIntroOpen = !isIntroOpen;
		if (isIntroOpen)
        {
			iTween.ScaleTo (DSDescription.instance.pnl_intro, new Vector3 (1, 1, 1), 0.5f);
            DSDescription.instance.Labels.SetActive(false);
        } else {
			iTween.ScaleTo (DSDescription.instance.pnl_intro, new Vector3 (0, 0, 0), 0.5f);
            DSDescription.instance.Labels.SetActive(true);
        }
		disableOther (WHAT_IS_SELECTED.INTRO);
	}


	// When ? is clicked on the main screen to show how to use this app
	public void openHelp ()
	{
		playbuttonClick ();
		isHelpOpen = !isHelpOpen;
		
if (isHelpOpen)
        {
            Debug.Log("Open");
			iTween.ScaleTo (DSDescription.instance.pnl_help, new Vector3 (1, 1, 1), 0.5f);
            DSDescription.instance.Labels.SetActive(false);
        }
        else
        {
            Debug.Log("close");
            iTween.ScaleTo (DSDescription.instance.pnl_help, new Vector3 (0, 0, 0), 0.5f);
            DSDescription.instance.Labels.SetActive(true);
        }
		disableOther (WHAT_IS_SELECTED.HELP);
	}

	// when a label is taped on the scene
	public void openInfo (string arg)
	{
		isInfoOpen = !isInfoOpen;
		playbuttonClick ();
		if (isInfoOpen) {
			iTween.ScaleTo (DSDescription.instance.pnl_Info, new Vector3 (1, 1, 1), 0.5f);
            DSDescription.instance.Labels.SetActive (false);
		} else {
			iTween.ScaleTo (DSDescription.instance.pnl_Info, new Vector3 (0, 0, 0), 0.5f);
            DSDescription.instance.Labels.SetActive (true);
		}

		if (arg != null) {
			for (int i = 0; i < DigestiveSystemInfo.info.Length / 2; i++) {
				if (arg.Trim () == DigestiveSystemInfo.info [i, 0]) {
                    DSDescription.instance.infoHeading.GetComponent<Text> ().text = "" + DigestiveSystemInfo.heading [i];
                    DSDescription.instance.infoDes.GetComponent<Text> ().text = "" + DigestiveSystemInfo.info [i, 1];
				}
			}
		}
		disableOther (WHAT_IS_SELECTED.INFO);
	}

	// To diable others panels when selected
	void disableOther (WHAT_IS_SELECTED arg)
	{
		if (arg == WHAT_IS_SELECTED.HELP) {
			isIntroOpen = false;
			isInfoOpen = false;
			iTween.ScaleTo (DSDescription.instance.pnl_intro, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (DSDescription.instance.pnl_Info, new Vector3 (0, 0, 0), 0.5f);
		} else if (arg == WHAT_IS_SELECTED.INTRO) {
			isHelpOpen = false;
			isInfoOpen = false;

			iTween.ScaleTo (DSDescription.instance.pnl_help, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (DSDescription.instance.pnl_Info, new Vector3 (0, 0, 0), 0.5f);
		} else if (arg == WHAT_IS_SELECTED.INFO) {
			isHelpOpen = false;
			isIntroOpen = false;

			iTween.ScaleTo (DSDescription.instance.pnl_help, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (DSDescription.instance.pnl_intro, new Vector3 (0, 0, 0), 0.5f);
		}
	}

    // Events trigger for Image Target detected added in the current reference
    // _ImageTargetDetected is in Reusable folder in the assets
    private void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += OnTargetDetection;
    }

    private void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= OnTargetDetection;
    }


    void OnTargetDetection(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
    {
		if (status)
        {
            DSDescription.instance.Labels = _transform.GetChild(0).transform.GetChild(2).gameObject;
            Debug.Log("detected");
			if (DSDescription.instance.pnl_NoImageTarget != null)
				iTween.ScaleTo (DSDescription.instance.pnl_NoImageTarget, new Vector3 (0, 0, 0), 0.25f);
            if (DSDescription.instance.parentCanvas != null)
            {
                Debug.Log("active");
                DSDescription.instance.parentCanvas.SetActive(true);
            }
            if (DSDescription.instance.Labels != null && !isIntroOpen && !isInfoOpen && !isHelpOpen)
                DSDescription.instance.Labels.SetActive (true);
		}
		if (!status)
        {
            Debug.Log("Not_detected");
            if (DSDescription.instance.pnl_NoImageTarget != null)
				iTween.ScaleTo (DSDescription.instance.pnl_NoImageTarget, new Vector3 (1, 1, 1), 0.25f);
			if (DSDescription.instance.parentCanvas != null)
                DSDescription.instance.parentCanvas.SetActive (false);
			if (DSDescription.instance.Labels != null)
                DSDescription.instance.Labels.SetActive (false);
		}
	}

	// Click sound
	void playbuttonClick ()
	{
        DSDescription.instance.GetComponent<AudioSource>().PlayOneShot(DSDescription.instance.buttonClick);
    }
}
