﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// While renaming the scipts refactor all the classes names with a prefix of ProjectName_
public class DSDescription : MonoBehaviour
{
    public static DSDescription instance;
    // parent of info , intro, help canvas to disable when image target is lost
    public GameObject parentCanvas;

	public GameObject pnl_intro;
	public GameObject pnl_help;
	public GameObject pnl_Info;

	public GameObject infoDes;
	public GameObject infoHeading;

	// Panle when image target is lost
	public GameObject pnl_NoImageTarget;

	public GameObject Labels;

	public AudioClip buttonClick;

    private void Start()
    {
        instance = this;
    }

    private void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += OnTargetDetection;
    }

    private void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= OnTargetDetection;
    }


    void OnTargetDetection(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
    {
        if (status)
        {
            DSDescription.instance.Labels = _transform.GetChild(0).transform.GetChild(2).gameObject;
            Debug.Log("detected");
            if (DSDescription.instance.pnl_NoImageTarget != null)
                iTween.ScaleTo(DSDescription.instance.pnl_NoImageTarget, new Vector3(0, 0, 0), 0.25f);
            if (DSDescription.instance.parentCanvas != null)
            {
                Debug.Log("active");
                DSDescription.instance.parentCanvas.SetActive(true);
            }
            if (DSDescription.instance.Labels != null && !DigestiveSystemController.instance.isIntroOpen && !DigestiveSystemController.instance.isInfoOpen && !DigestiveSystemController.instance.isHelpOpen)
                DSDescription.instance.Labels.SetActive(true);
        }
        if (!status)
        {
            Debug.Log("Not_detected");
            if (DSDescription.instance.pnl_NoImageTarget != null)
                iTween.ScaleTo(DSDescription.instance.pnl_NoImageTarget, new Vector3(1, 1, 1), 0.25f);
            if (DSDescription.instance.parentCanvas != null)
                DSDescription.instance.parentCanvas.SetActive(false);
            if (DSDescription.instance.Labels != null)
                DSDescription.instance.Labels.SetActive(false);
        }
    }
    public void close_info()
    {
        DigestiveSystemController.instance.openInfo("");
    }

    public void opn_intro()
    {
        DigestiveSystemController.instance.opnIntro();
    }
    public void opn_help()
    {
        DigestiveSystemController.instance.openHelp();
    }
}
