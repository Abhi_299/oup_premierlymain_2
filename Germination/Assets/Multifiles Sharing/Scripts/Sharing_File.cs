﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Sharing_File : MonoBehaviour {
    private string Path = "class_02_evs_parts_of_body.pdf";

    void Start()
    {
        FilePath = Application.persistentDataPath+"/"+ Path;
        Debug.Log(FilePath);
    }

    public string FilePath { get; set; }

    public void SharingPDF()
    {
        StartCoroutine(ShareFile(FilePath));
    }

    IEnumerator ShareFile(string pdfLocalPath)
    {
        Debug.Log("SHARE : " + pdfLocalPath);
        Byte[] pdfBytes; 
        yield return pdfBytes =File.ReadAllBytes(pdfLocalPath);
		Debug.Log(pdfBytes == null);
        if (pdfBytes!=null)
        {
            Debug.Log("pdf bytes " + pdfBytes.Length);
            Sharing.share.files.Clear();
            Sharing.share.files.Add(pdfBytes);
            Debug.Log("on share start");
            Sharing.share.OnShare(pdfLocalPath);
        }
        yield return null;
    }

    private Byte[] Getfile(string path)
    {
        TextAsset pdfAsset;
        pdfAsset = Resources.Load("class_02_evs_parts_of_body.pdf", typeof(TextAsset)) as TextAsset;
        Debug.Log(pdfAsset==null);
        if (pdfAsset != null)
        {
             return pdfAsset.bytes;
        }
        return null;
    }
}
