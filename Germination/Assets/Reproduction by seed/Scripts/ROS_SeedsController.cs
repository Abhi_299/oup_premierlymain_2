﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;
using UnityEngine.UI;


namespace Germination
{
	public class ROS_SeedsController : MonoBehaviour
	{
		public enum SeedProperty
		{
			None,
			SeedActivity,
			SeedGermination
		};
		private SeedProperty _seedProperty;


		[HideInInspector] public Animator seedsAnimator; 
		public Animator panelAnimator, welcomeTextAnimator;
        [HideInInspector]
		public GameObject labels;

		public Text daysText;
		public GameObject openSeedBtn, closeSeedBtn, germinationBtn, homeBtn, labelName, description, infoPanel, 
			sun, waterCane, labelGermination, germinationPanel,closeBtnInfo;
		public Slider slider;
		public ROS_SeedInfo RosSeedInfo;
		public static ROS_SeedsController instance;

		public GameObject _Hud, wizarLogo;
		public RectTransform infoLowerPanel;
		
		
		// Use this for initialization
		void Start ()
		{
			_seedProperty = SeedProperty.None;
			instance = this;
			welcomeTextAnimator.SetBool("isopen", true);
			
		}

		private void OnEnable()
		{
			LoadModelOnDetection.isModelLoaded += HandleModelLoaded;
		}

		private void OnDisable()
		{
			LoadModelOnDetection.isModelLoaded -= HandleModelLoaded;
		}

		// Update is called once per frame
		void Update () {
			if (Input.touchCount==2 ||  Input.GetMouseButton(0))
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit))
				{
					GameObject obj = hit.transform.gameObject;
					Debug.Log(obj.name);
					OpenSeeds(true);
				}
			}
			if (Input.touchCount==1 ||  Input.GetMouseButton(0))
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit))
				{
					GameObject obj = hit.transform.gameObject;
					Debug.Log(obj.name);
					OpenInfo(obj.name);
				}
			}
		}

		#region  delegate handler 

		void HandleModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
		{
			GameObjectUIOnOff(openSeedBtn,status);
			GameObjectUIOnOff(closeSeedBtn,false);
			GameObjectUIOnOff(homeBtn,false);
			GameObjectUIOnOff(slider.gameObject,false);
			GameObjectUIOnOff(germinationBtn,status);
		    GameObjectUIOnOff(infoPanel,status);
		    GameObjectUIOnOff(waterCane,false);
		    GameObjectUIOnOff(sun	,false);
			if (status && sceneType==LoadModelOnDetection.SceneType.AR)
			{
				_seedProperty = SeedProperty.SeedActivity;
				ROS_DetectedObjectAssets.instance.seed.SetActive(true);
				ROS_DetectedObjectAssets.instance.labelCanvas.SetActive(false);
				ROS_DetectedObjectAssets.instance.plantGrowth.SetActive(false);
				seedsAnimator = ROS_DetectedObjectAssets.instance.seed.GetComponent<Animator>();
				labels = ROS_DetectedObjectAssets.instance.labelCanvas;
				openSeedBtn.GetComponent<Button>().interactable = status;
				closeSeedBtn.GetComponent<Button>().interactable = status;
			}
			else
			{
				

			}
			welcomeTextAnimator.SetBool("isopen", false);

			if (!status) {
				infoLowerPanel.pivot = new Vector2 (0.5f,1);
				closeBtnInfo.SetActive (false);
			}
		}

		#endregion
		
		public void OpenSeeds(bool status)
		{
			if (_seedProperty == SeedProperty.SeedActivity)
			{
				if (seedsAnimator != null)
					seedsAnimator.SetBool("OpenSeed", status);
				float time = seedsAnimator.runtimeAnimatorController.animationClips[0].length;
				Debug.Log(time + " status " + status);
				StopCoroutine("ActivateLabel");

				if (status)
				{
					_seedProperty = SeedProperty.SeedActivity;
					
					ROS_DetectedObjectAssets.instance.plantGrowth.GetComponent<Animator>().speed = 0;
					openSeedBtn.GetComponent<Button>().interactable = !status;
					closeSeedBtn.GetComponent<Button>().interactable = status;
					StartCoroutine(ActivateLabel(true, time));

				}
				else
				{
					openSeedBtn.GetComponent<Button>().interactable = !status;
					closeSeedBtn.GetComponent<Button>().interactable = status;
					StartCoroutine(ActivateLabel(false, time));
				}

			}
		}

		public void GerminationClicked()
		{
			_seedProperty = SeedProperty.SeedGermination;
			if (_seedProperty == SeedProperty.SeedGermination)
			{
				
				OpenSeeds(false);
				slider.gameObject.SetActive(false);
				ROS_DetectedObjectAssets.instance.seed.SetActive(false);
				ROS_DetectedObjectAssets.instance.plantGrowth.SetActive(true);
//				ROS_DetectedObjectAssets.instance.textGermination.SetActive(true);
				ROS_DetectedObjectAssets.instance.labelCanvas.SetActive(false);
				StopCoroutine("ActivateLabel");
				
				openSeedBtn.GetComponent<Button>().interactable = true;
				closeSeedBtn.GetComponent<Button>().interactable = true;
				GameObjectUIOnOff(germinationBtn,false);
				IsGermnationStart(true);
				labelGermination.transform.GetChild(0).GetComponent<Text>().text = "Tap on Sun Button";
				infoPanel.SetActive (false);
				infoLowerPanel.pivot = new Vector2 (0.5f,1);
				closeBtnInfo.SetActive (false);
				panelAnimator.SetBool ("isOpen",false);
				slider.value = 0f;
			}
		}

		public void  OpenInfo(string buttonName)
		{
			Seedinfo seedinfo = RosSeedInfo.SeedInfos.Find(item => item.labelName.ToLower().Trim() == buttonName.ToLower().Trim());
			if (seedinfo != null)
			{
				labelName.GetComponent<Text>().text = seedinfo.labelName;
				description.GetComponent<Text>().text = seedinfo.desc;
				OpenCloseAnimation( true);
			}
		}
		
		public	void OpenCloseAnimation(bool status)
		{
			panelAnimator.SetBool("isOpen", status);
		}

		public void PlantGrowthSilder()
		{
			daysText.text =((int)slider.value).ToString()+" days";
			ROS_DetectedObjectAssets.instance.plantGrowth.GetComponent<Animator>().SetFloat("plantGrowth",slider.normalizedValue);
//			ROS_DetectedObjectAssets.instance.textGermination.GetComponent<Animator>().SetFloat("textGermination",slider.normalizedValue);
		}

		public void WaterButtonClicked()
		{
			ROS_DetectedObjectAssets.instance.waterCane.SetActive(true);
			GameObjectUIOnOff(waterCane,false);
			GameObjectUIOnOff(slider.gameObject,true);
			labelGermination.transform.GetChild(0).GetComponent<Text>().text = "Use Slider for Plant Growth";
			CancelInvoke("OffWaterCane");
			Invoke("OffWaterCane",5f);
		}

		void OffWaterCane()
		{
			if (ROS_DetectedObjectAssets.instance != null) {
				ROS_DetectedObjectAssets.instance.waterCane.SetActive(false);
			}
		}
		
		public void SunlightButtonClicked()
		{
			ROS_DetectedObjectAssets.instance.sun.SetActive(true);
			GameObjectUIOnOff(sun,false);
			GameObjectUIOnOff(waterCane,true);
			labelGermination.transform.GetChild(0).GetComponent<Text>().text = "Tap on Water Button";
		}

		public void HomeButton()
		{
			_seedProperty = SeedProperty.SeedActivity;
			if (seedsAnimator != null)
				seedsAnimator.SetBool("OpenSeed", false);
			ROS_DetectedObjectAssets.instance.seed.SetActive(true);
			ROS_DetectedObjectAssets.instance.sun.SetActive(false);
			ROS_DetectedObjectAssets.instance.waterCane.SetActive(false);
			ROS_DetectedObjectAssets.instance.plantGrowth.SetActive(false);
//			ROS_DetectedObjectAssets.instance.textGermination.SetActive(false);
			ROS_DetectedObjectAssets.instance.labelCanvas.SetActive(false);
			StopCoroutine("ActivateLabel");
				
			GameObjectUIOnOff(germinationBtn,true);
			IsGermnationStart(false);
			GameObjectUIOnOff(slider.gameObject,false);
			GameObjectUIOnOff(waterCane,false);
			infoPanel.SetActive (true);
		}
		
		IEnumerator  ActivateLabel(bool status, float time)
		{
			
			yield return new WaitForSeconds(time);
			if (_seedProperty == SeedProperty.SeedActivity)
			{
				if (labels)
					labels.SetActive(status);
				if (status)
				{

					GameObjectUIOnOff(openSeedBtn, !status);
					GameObjectUIOnOff(closeSeedBtn, status);
				}
				else
				{
					GameObjectUIOnOff(openSeedBtn, !status);
					GameObjectUIOnOff(closeSeedBtn, status);
				}
			}

		}

		void GameObjectUIOnOff(GameObject uiObject,bool status)
		{
			if (uiObject != null)
			{
				uiObject.SetActive(status);
			}
		}

		void IsGermnationStart(bool status)
		{
			GameObjectUIOnOff(germinationPanel,status);
			GameObjectUIOnOff(waterCane,!status);
			GameObjectUIOnOff(sun,status);
			GameObjectUIOnOff(openSeedBtn,!status);
			GameObjectUIOnOff(closeSeedBtn,false);
			GameObjectUIOnOff(homeBtn,status);
			GameObjectUIOnOff(slider.gameObject,!status);
			GameObjectUIOnOff(labelGermination,status);
		}


		public void ScreenHot_UIManager(){
		
			StartCoroutine (ScreenShotUI());
		}

		IEnumerator ScreenShotUI(){
			_Hud.SetActive (false);
			wizarLogo.SetActive (true);
			yield return new WaitForSeconds (3.5f);
			_Hud.SetActive (true);
			wizarLogo.SetActive (false);
		}

	}


}
