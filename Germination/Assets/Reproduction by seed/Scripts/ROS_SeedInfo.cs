﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Germination
{
	[System.Serializable]
	public class ROS_SeedInfo
	{
	    public  List<Seedinfo> SeedInfos= new List<Seedinfo>();
	}

	[System.Serializable]
    public class Seedinfo
	{
		public  string labelName;
		public  string desc;
	}
}
