﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Organs_ModuleManager : MonoBehaviour {

	public List<LabelsInst> labelInst;
	public Text organInstructionText, organNametext;
	public GameObject instPanel;
	public GameObject backBtn;
	public GameObject uiPanels, wizarLogo, searchPanel;

	Transform currObject;
	bool isDetected;

	// Use this for initialization
	void Start () {
		
	}

	void OnEnable() {
		LoadModelOnDetection.isModelLoaded += Model_Loaded;
		InputTouchHit.OnScreenTouch += OnLabelClick;
	}

	void OnDisable() {
		LoadModelOnDetection.isModelLoaded -= Model_Loaded;
		InputTouchHit.OnScreenTouch -= OnLabelClick;
	}

	public void Model_Loaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType) {
		StartCoroutine (ModelLoading(_transform, status));
	}

	IEnumerator ModelLoading(Transform _transform, bool status) {
		if (!status) {
			Organs_ZoomInOut.instance.isModlDetected = false;
			isDetected = false;
			if (currObject != null) {
				Click_BackBtn ();
			}
		}

		searchPanel.SetActive (!status);
		yield return new WaitForEndOfFrame ();

		if (status) {
			currObject = _transform.GetChild (0);
			isDetected = true;
			Organs_ZoomInOut.instance.currModel = currObject.gameObject;
			Organs_ZoomInOut.instance.isModlDetected = true;
		}

	}

	public void OnLabelClick(RaycastHit _hit) {

		for (int i = 0; i < labelInst.Count; i++) {
			if (_hit.transform.name.ToLower() == labelInst [i].organName.ToLower()) {
				organInstructionText.text = labelInst [i].organInstruction;
				organNametext.text = labelInst [i].organName;
				instPanel.SetActive (true);
				currObject.GetComponent<Organs_Container> ().humanoid_MainBody.SetActive (false);
				currObject.GetComponent<Organs_Container> ().humanoid_BodyParts.SetActive (true);
				currObject.GetComponent<Organs_Container> ().humanoid_Labels.SetActive (false);

				currObject.GetComponent<Organs_Container> ().humanOrgans [i].SetActive (true);
				StartCoroutine (MoveTarget(currObject.GetComponent<Organs_Container> ().humanOrgans [i]));
				break;
			}
		}

	}

	IEnumerator MoveTarget(GameObject _object ){
		float _moveTime = 0;

		while (_moveTime < 1) {
			_moveTime += 0.02f;
			_object.transform.localScale = Vector3.Lerp (Vector3.one,Vector3.one*4,_moveTime);

			yield return new WaitForSeconds (0.01f);

			if (!isDetected) {
				yield break;
			}
		}

		if (isDetected) {
			backBtn.SetActive (true);
		}
	}


	public void Click_BackBtn() {
		backBtn.SetActive (false);
		instPanel.SetActive (false);
		currObject.GetComponent<Organs_Container> ().humanoid_MainBody.SetActive (true);
		currObject.GetComponent<Organs_Container> ().humanoid_BodyParts.SetActive (false);
		currObject.GetComponent<Organs_Container> ().humanoid_Labels.SetActive (true);

		for (int i = 0; i < labelInst.Count; i++) {
			currObject.GetComponent<Organs_Container> ().humanOrgans [i].transform.localScale = Vector3.one;
			currObject.GetComponent<Organs_Container> ().humanOrgans [i].SetActive (false);
		}
	}

	public void Click_captureBtn() {
		StartCoroutine (Uimanager());
	}

	IEnumerator Uimanager() {
		uiPanels.SetActive (false);
		wizarLogo.SetActive (true);
		yield return new WaitForSeconds (3.5f);
		uiPanels.SetActive (true);
		wizarLogo.SetActive (false);
	}

}

[System.Serializable]
public class LabelsInst{
	public string organName;
	public string organInstruction;
}
