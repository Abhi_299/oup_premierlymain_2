﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatesOfMatter_UIController : MonoBehaviour {

	GameObject _CurrObject;
	StatesOfMatter_ObjectManager _CurrObjScript;
	bool isStatesBtn;

	public GameObject infoPanel;
	public Text infoPanelTxt, activityPanelText;
	public Slider tempSlider;
	public Material iceMaterial;
	public GameObject homeBtn;
	float preTemp = 5;
	public Animator infoPanelAnim;
	public GameObject uiPanels,wizarLogo;
	public GameObject infoClose;
	public GameObject searchPanel;
	public Animator startPanel;

	bool isInfoEnable,isStartPanel=true, isModelDetected;
	// Use this for initialization
	void Start () {
		startPanel.SetBool ("CloseInst",false);
		startPanel.gameObject.SetActive (true);	
		isStartPanel = true;
		StartCoroutine (Disable_Start());
	}

	IEnumerator Disable_Start(){
		yield return new WaitForSeconds (2);
		startPanel.SetBool ("CloseInst",true);
		yield return new WaitForSeconds (1);
		startPanel.gameObject.SetActive (false);
		isStartPanel = false;
		yield return new WaitForSeconds (0.5f);
		if (!isModelDetected) {
			searchPanel.SetActive (true);
		}
	}
	void OnEnable() {
		LoadModelOnDetection.isModelLoaded += DetectedTarget;
		InputTouchHit.OnScreenTouch += ClickOn_StatesofMatter;
		InputTouchHit.OnScreenTouch += Click_StatesBtns;
		InputTouchHit.OnScreenTouch += Click_ChangingStateBtn;
	}

	void OnDisable() {
		LoadModelOnDetection.isModelLoaded -= DetectedTarget;
		InputTouchHit.OnScreenTouch -= ClickOn_StatesofMatter;
		InputTouchHit.OnScreenTouch -= Click_StatesBtns;
		InputTouchHit.OnScreenTouch -= Click_ChangingStateBtn;
	}


	public void DetectedTarget(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType scenetype) {
	
		isModelDetected = status;
		StartCoroutine (Detected_Target(status, _transform));
	}

	IEnumerator Detected_Target(bool status, Transform _transform){

		if (!status) {
			isStatesBtn = false;
			tempSlider.gameObject.SetActive (false);
			homeBtn.SetActive (false);
			preTemp = 5;
			StatesOfMatter_ZoomInOut.instance.isModlDetected = false;

//			isInfoEnable = false;
//			infoPanelAnim.SetBool ("EnableInfo", false);
//			infoPanelAnim.SetBool ("DisableInfo", true);
//			infoClose.SetActive (false);
			if(!isStartPanel) {
				searchPanel.SetActive (true);
			}

			infoPanel.SetActive (false);

		}
		yield return new WaitForEndOfFrame ();
		if (status) {
			searchPanel.SetActive (false);
			_CurrObject = _transform.GetChild (0).gameObject;
			_CurrObjScript = _CurrObject.transform.GetComponent<StatesOfMatter_ObjectManager> ();
			StatesOfMatter_ZoomInOut.instance.currModel = _CurrObject;
			StatesOfMatter_ZoomInOut.instance.isModlDetected = true;
		}
	}

	#region States of matter
	public void ClickOn_StatesofMatter(RaycastHit hit) {

		if (hit.transform.parent.name == _CurrObjScript.statesOfMatter.name) {
			isStatesBtn = !isStatesBtn;

			if (isStatesBtn) {
				_CurrObjScript.statesOfMatter_Btns.SetActive (true);
				_CurrObjScript.statesOfMatter_Btns.GetComponent<Animator> ().SetBool ("EnableBtn",true);
				_CurrObjScript.changingStatesOfMatter.SetActive (false);
				_CurrObjScript.statesOfMatter.transform.localPosition = new Vector3 (-0.5f,0.23f,0);
				_CurrObjScript.geyser.SetActive (true);
			} else {
				_CurrObjScript.statesOfMatter_Btns.GetComponent<Animator> ().SetBool ("EnableBtn",false);

				_CurrObjScript.changingStatesOfMatter.SetActive (true);
				_CurrObjScript.solidLabel.SetActive (false);
				_CurrObjScript.liquidLabel.SetActive (false);
				_CurrObjScript.gasLabel.SetActive (false);
				_CurrObjScript.statesOfMatter.transform.localPosition = new Vector3 (0.2f,0.23f,0);
				_CurrObjScript.geyser.SetActive (false);

			}
			infoPanel.SetActive (false);
			homeBtn.SetActive (true);
		}
	}

	public void Click_StatesBtns(RaycastHit hit) {
	
		if (isStatesBtn) {
			_CurrObjScript.changingStatesOfMatter.SetActive (false);
			_CurrObjScript.solidLabel.SetActive (false);
			_CurrObjScript.liquidLabel.SetActive (false);
			_CurrObjScript.gasLabel.SetActive (false);
		}

		if (hit.transform.gameObject.name == "Solid") {
			_CurrObjScript.solidLabel.SetActive (true);
			_CurrObjScript.liquidLabel.SetActive (false);
			_CurrObjScript.gasLabel.SetActive (false);

			_CurrObjScript.statesOfMatter.transform.localPosition = new Vector3 (-0.5f,0.2f,-0.36f);
			infoPanelTxt.text = StatesOfMatter_InformationManager.instance.solid_Information;
			activityPanelText.text = StatesOfMatter_InformationManager.instance.solidActivity;
			infoPanel.SetActive (true);
		}
		else if (hit.transform.gameObject.name == "Liquid") {
			_CurrObjScript.liquidLabel.SetActive (true);
			_CurrObjScript.solidLabel.SetActive (false);
			_CurrObjScript.gasLabel.SetActive (false);

			_CurrObjScript.statesOfMatter.transform.localPosition = new Vector3 (-0.5f,0.2f,-0.36f);
			infoPanelTxt.text = StatesOfMatter_InformationManager.instance.liquid_Information;
			activityPanelText.text = StatesOfMatter_InformationManager.instance.liquidActivity;
			infoPanel.SetActive (true);
		}
		else if (hit.transform.gameObject.name == "Gas") {
			_CurrObjScript.gasLabel.SetActive (true);
			_CurrObjScript.solidLabel.SetActive (false);
			_CurrObjScript.liquidLabel.SetActive (false);

			_CurrObjScript.statesOfMatter.transform.localPosition = new Vector3 (-0.5f,0.2f,-0.36f);
			infoPanelTxt.text = StatesOfMatter_InformationManager.instance.gas_Information;
			activityPanelText.text = StatesOfMatter_InformationManager.instance.gasActivity;
			infoPanel.SetActive (true);
		}


	}
		
	#endregion

	#region Changing states of matter

	public void Click_ChangingStateBtn(RaycastHit hit) {
	
		if (hit.transform.parent.name == _CurrObjScript.changingStatesOfMatter.name) {
			_CurrObjScript.changingStatesOfMatter.SetActive (false);
			_CurrObjScript.statesOfMatter.SetActive (false);
			tempSlider.gameObject.SetActive (true);
			tempSlider.value = 5;
//			iceMaterial.SetColor ("_EmissionColor",new Color(0,0,0));
//			iceMaterial.SetFloat ("_Ice_fresnel",3);
			_CurrObjScript.iceObject.GetComponent<SkinnedMeshRenderer>().materials[0].SetFloat ("_Ice_fresnel",3);

			_CurrObjScript.glassContainer.SetBool ("Boil", false);
			_CurrObjScript.glassContainer.speed = 1;
			_CurrObjScript.glassContainer.SetFloat ("WaterLevel",0);
			_CurrObjScript.glassContainer.gameObject.SetActive (true);
			_CurrObjScript.burner.SetActive (true);
			_CurrObjScript.smokeEffect.startLifetime = 2;
			infoPanel.SetActive (false);
			homeBtn.SetActive (true);


		}
	}

	public void Change_TempSlider() {

		infoPanel.SetActive (true);

		if (tempSlider.value < 5) {
			if (tempSlider.value < 3) {
				_CurrObjScript.boilBubble.SetActive (true);
				_CurrObjScript.boilSurfaceBubble.SetActive (true);
			} else {
				_CurrObjScript.boilBubble.SetActive (false);
				_CurrObjScript.boilSurfaceBubble.SetActive (false);
			}
			_CurrObjScript.fireEffect.SetActive (true);
			_CurrObjScript.glassContainer.SetBool ("Boil", true);
			_CurrObjScript.glassContainer.SetFloat ("WaterLevel",(5-tempSlider.value)*0.03f);
			_CurrObjScript.smokeEffect.gameObject.SetActive (true);
			_CurrObjScript.smokeEffect.startLifetime = (5-tempSlider.value)*0.2f+2f;

			if (tempSlider.value < preTemp) {
				infoPanelTxt.text = StatesOfMatter_InformationManager.instance.waterHeat_Info;
				activityPanelText.text = StatesOfMatter_InformationManager.instance.waterHeatActivity;
			} else {
				infoPanelTxt.text = StatesOfMatter_InformationManager.instance.steamCool_Info;
				activityPanelText.text = StatesOfMatter_InformationManager.instance.steamCoolActivity;
			}
			preTemp = tempSlider.value;

		} else {

			if (tempSlider.value > 7) {
				_CurrObjScript.snowFall.SetActive (true);
			} else {
				_CurrObjScript.snowFall.SetActive (false);
			}
			_CurrObjScript.glassContainer.SetBool ("Boil", false);
			_CurrObjScript.glassContainer.speed = (10-tempSlider.value) * 0.25f;

			float emissionValue = (10 - tempSlider.value)*3/5;
//			iceMaterial.SetColor ("_EmissionColor",new Color(emissionValue,emissionValue,emissionValue));
//			iceMaterial.SetFloat ("_Ice_fresnel",emissionValue);
			_CurrObjScript.iceObject.GetComponent<SkinnedMeshRenderer>().materials[0].SetFloat ("_Ice_fresnel",emissionValue);

			_CurrObjScript.fireEffect.SetActive (false);
			_CurrObjScript.boilBubble.SetActive (false);
			_CurrObjScript.boilSurfaceBubble.SetActive (false);
			_CurrObjScript.smokeEffect.gameObject.SetActive (false);

			if (tempSlider.value < preTemp) {
				infoPanelTxt.text = StatesOfMatter_InformationManager.instance.iceHeat_Info;
				activityPanelText.text = StatesOfMatter_InformationManager.instance.iceHeatActivity;
			} else {
				infoPanelTxt.text = StatesOfMatter_InformationManager.instance.waterCool_Info;
				activityPanelText.text = StatesOfMatter_InformationManager.instance.waterCoolActivity;
			}
			preTemp = tempSlider.value;
		}

	}

	#endregion

	public void Click_HomeBtn(){
	
		homeBtn.SetActive (false);
		if (_CurrObjScript.statesOfMatter_Btns.GetComponent<Animator> ().GetBool ("EnableBtn")) {
			_CurrObjScript.statesOfMatter_Btns.GetComponent<Animator> ().SetBool ("EnableBtn", false);
		}
		_CurrObjScript.statesOfMatter_Btns.SetActive (false);
		_CurrObjScript.statesOfMatter.SetActive (true);
		_CurrObjScript.changingStatesOfMatter.SetActive (true);
		_CurrObjScript.solidLabel.SetActive (false);
		_CurrObjScript.liquidLabel.SetActive (false);
		_CurrObjScript.gasLabel.SetActive (false);
		_CurrObjScript.geyser.SetActive (false);
		_CurrObjScript.statesOfMatter.transform.localPosition = new Vector3 (0.2f,0.23f,0);

		isStatesBtn = false;
		tempSlider.gameObject.SetActive (false);

		tempSlider.gameObject.SetActive (false);
		_CurrObjScript.glassContainer.gameObject.SetActive (false);
		_CurrObjScript.burner.SetActive (false);
		_CurrObjScript.boilBubble.SetActive (false);
		_CurrObjScript.boilSurfaceBubble.SetActive (false);
		_CurrObjScript.fireEffect.SetActive (false);
		_CurrObjScript.smokeEffect.gameObject.SetActive (false);
		_CurrObjScript.snowFall.SetActive (false);

		preTemp = 5;

//		isInfoEnable = false;
//		infoPanelAnim.SetBool ("EnableInfo", false);
//		infoPanelAnim.SetBool ("DisableInfo", true);
//		infoClose.SetActive (false);

		infoPanel.SetActive (false);

	}

	public void ScreenShotUiManager(){
		StartCoroutine (ScreenShotUi());
	}

	IEnumerator ScreenShotUi(){
		uiPanels.SetActive (false);
		wizarLogo.SetActive (true);
		startPanel.SetBool ("CloseInst",false);
		startPanel.gameObject.SetActive (false);
		isStartPanel = false;
		StopCoroutine ("Disable_Start");
		yield return new WaitForSeconds (3.5f);
		uiPanels.SetActive (true);
		wizarLogo.SetActive (false);
	}


	public void Click_OnInfoBtn(){
		isInfoEnable = !isInfoEnable;

		infoPanelAnim.SetBool ("EnableInfo", isInfoEnable);
		infoPanelAnim.SetBool ("DisableInfo", !isInfoEnable);
		infoClose.SetActive (isInfoEnable);

	}
}
