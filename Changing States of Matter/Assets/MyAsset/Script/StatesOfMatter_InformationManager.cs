﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatesOfMatter_InformationManager : MonoBehaviour {

	public static StatesOfMatter_InformationManager instance;

	public string solid_Information, liquid_Information, gas_Information;
	public string iceHeat_Info, waterHeat_Info, steamCool_Info, waterCool_Info;
	public string solidActivity, liquidActivity, gasActivity, iceHeatActivity, waterHeatActivity, steamCoolActivity, waterCoolActivity;

	void Start(){
		instance = this;
	}
}
