﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatesOfMatter_WaterMover : MonoBehaviour {

	public Material liquidWater;
	public float speed;

	float currSpeed;

	void Update() {
		currSpeed += speed * Time.deltaTime;
		liquidWater.mainTextureOffset = new Vector2 (0,-currSpeed);
	}
}
