﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatesOfMatter_ObjectManager : MonoBehaviour {

	public GameObject statesOfMatter,changingStatesOfMatter;
	public GameObject statesOfMatter_Btns;
	public GameObject solidLabel,liquidLabel,gasLabel;
	public Animator glassContainer;
	public GameObject boilBubble,boilSurfaceBubble,fireEffect,burner,snowFall;
	public ParticleSystem smokeEffect;
	public GameObject iceObject,geyser;
}
