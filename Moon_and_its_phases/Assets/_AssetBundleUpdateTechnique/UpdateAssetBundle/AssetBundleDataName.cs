﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AssetBundleDataName", menuName = "WizAr/Create AssetBundle Name", order = 1)]
public class AssetBundleDataName : ScriptableObject
{
	    public string[] dataExtension;
}


