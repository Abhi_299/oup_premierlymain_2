﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAIP_LogoScript : MonoBehaviour {

    public GameObject LeftMenuPanel;
    public GameObject GameUI;
    public GameObject btnBack;
    public GameObject wizarLogo;


    public void WizArLogoManager()
    {
        wizarLogo.SetActive(true);
        ActivateUiOnCapture(false);
        Invoke("DisableWizARLogo", 4);
    }

    void DisableWizARLogo()
    {
        wizarLogo.SetActive(false);
        ActivateUiOnCapture(true);
    }

    void ActivateUiOnCapture(bool status)
    {
        btnBack.SetActive(status);
        LeftMenuPanel.SetActive(status);
        GameUI.SetActive(status);
    }
}
