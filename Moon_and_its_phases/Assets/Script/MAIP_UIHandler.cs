﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MAIP_UIHandler : MonoBehaviour {

    public static MAIP_UIHandler instance;

    public GameObject info_Btn;
    public GameObject PhasesOfMoon_Btn;
    public GameObject ViewFromEarth_Btn;
    public GameObject NormalView_Btn;
    public GameObject searching_Icon;
    public GameObject info_moon;
    public GameObject Phases_Btn;
    public Text MoonName;
    public Text Descripton_Text;
    public GameObject Description_Pannel;
    public GameObject PhasesBtn;
    public GameObject DescriptionCrossBtn;
 

    // Use this for initialization
    void Awake ()
    {
        instance = this;
        //searching_Icon.SetActive(true);
    }
}
