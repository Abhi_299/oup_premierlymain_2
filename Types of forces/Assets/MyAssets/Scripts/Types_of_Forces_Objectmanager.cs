﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Types_of_Forces_Objectmanager : MonoBehaviour {

	public GameObject boy_Idle, boy_Push, boy_Pull, boy_GravitationalForce, boy_MuscularForce, magneticeForce;
	public AnimationClip pushClip, pullClip, gravityClip, muscularClip, magneticClip;
	public GameObject frictionArrow;
	public float boyPullSpeed,boyPushSpeed;
	internal bool stopBoy;
	// Use this for initialization
	void Start () {
		
	}

	void Update() {

		if (stopBoy) {
			return;
		}

		if (!boy_Pull.activeInHierarchy && !boy_Push.activeInHierarchy) {
			boy_Push.transform.localPosition = new Vector3 (-0.2f,0,0);
			boy_Pull.transform.localPosition = Vector3.zero;
			return;
		}

		if (boy_Pull.activeInHierarchy) {
			boy_Pull.transform.localPosition = new Vector3 (boy_Pull.transform.localPosition.x + boyPullSpeed * Time.deltaTime, 0, 0);
		} else if (boy_Push.activeInHierarchy) {
			boy_Push.transform.localPosition = new Vector3 (boy_Push.transform.localPosition.x + boyPushSpeed * Time.deltaTime, 0, 0);
		}



	}
	

}
