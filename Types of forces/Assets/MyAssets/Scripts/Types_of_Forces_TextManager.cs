﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Types_of_Forces_TextManager : MonoBehaviour {

	public static Types_of_Forces_TextManager instance;

	public string inst_PushActivity, inst_PullActivity, info_ForcesActivity;
	public string inst_GravitationalForce,inst_FrictionalForce,inst_MagneticForce,inst_MusculurForce;
	public string inst_GravityActivity, inst_FrictionActivity, inst_MagneticActivity, inst_MusculurActivity;

	// Use this for initialization
	void Awake () {
		instance = this;
	}
	

}
