﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Types_of_Forces_SoundManager : MonoBehaviour {

	public AudioSource audioPlayer;

	// Use this for initialization
	void Start () {
		
	}

	public void PlayVoice(AudioClip _clip){
		audioPlayer.PlayOneShot (_clip);
	}
	

}
