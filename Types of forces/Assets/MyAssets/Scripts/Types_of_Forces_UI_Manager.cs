﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Types_of_Forces_UI_Manager : MonoBehaviour {

    public GameObject backBtn, captureBtn, forcesBtn, typesOfForcesBtn, homeBtn,uiPanels;
    public GameObject pushBtn, pullBtn, gravitationalForce_Btn, frictionForce_Btn, muscularForce_Btn, magneticForce_Btn;
	public GameObject Activity_Instpanel,infoPanel,wizarLogo, infoPanelCloseBtn;
	public Text forcesActivity_Txt,infoPanelTxt;
	public GameObject searchPanel;

	Coroutine _coroutine;
	Transform _currModel;
	bool isModelDetected,isInfoEnable;
	bool isForceActivity, isTypesOfforceActivity;


	// Use this for initialization
	void Start () {
		
	}

	void OnEnable()
	{
		LoadModelOnDetection.isModelLoaded += OnModelload;
	}

	void OnDisable(){
		LoadModelOnDetection.isModelLoaded -= OnModelload;
//		if (_coroutine != null) {
//			StopCoroutine (_coroutine);
//		}
	}

	public void OnModelload(string _name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType) {

		if (_coroutine != null) {
			StopCoroutine (_coroutine);
		}
		isModelDetected = status;
		if (!status) {
			Types_of_Forces_ZoomInOut.instance.isModlDetected = false;
			searchPanel.SetActive (true);
		}
		_coroutine = StartCoroutine (LoadOnDetection(status,_transform));
    }


	IEnumerator LoadOnDetection(bool status,Transform _transform) {
		
		yield return new WaitForEndOfFrame ();
		ActivateStartBtn (status);
		DisableAllBtns ();

		if (status) {
			_currModel = _transform.GetChild (0);
			Types_of_Forces_ZoomInOut.instance.currModel = _currModel.gameObject;
			Types_of_Forces_ZoomInOut.instance.isModlDetected = true;
			searchPanel.SetActive (false);
			isForceActivity = false;
			isTypesOfforceActivity = false;
		} else {
			CancelInvoke ("AfterPushActivity");
			CancelInvoke ("AfterPullActivity");

			CancelInvoke ("AfterGravitationalActivity");
			CancelInvoke ("AfterFrictionForceActivity");
			CancelInvoke ("AfterMuscularForceActivity");
			CancelInvoke ("AfterMagneticForceActivity");

		}
	
	}

	//To enable force and types of forces btn
	void ActivateStartBtn(bool status){
		forcesBtn.SetActive (status);
		typesOfForcesBtn.SetActive (status);
	}


	public void ClickOn_ForcesBtn(bool status) {
		homeBtn.SetActive (status);
		infoPanel.SetActive (status);
		pushBtn.SetActive (status);
		pullBtn.SetActive (status);
		ActivateStartBtn (!status);
		infoPanelTxt.text = Types_of_Forces_TextManager.instance.info_ForcesActivity;
	}

	public void ClickOn_HomeBtn(){

		if (!isForceActivity && !isTypesOfforceActivity) {
			ActivateStartBtn (true);
			DisableAllBtns ();
		} else if (isForceActivity) {
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (true);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Push.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Pull.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Push.transform.localPosition = new Vector3 (-0.2f,0,0);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Pull.transform.localPosition = Vector3.zero;
			Activity_Instpanel.SetActive (false);
			pushBtn.SetActive (true);
			pullBtn.SetActive (true);

			isForceActivity = false;
		} else if (isTypesOfforceActivity) {
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (true);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_MuscularForce.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_GravitationalForce.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Push.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().frictionArrow.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().magneticeForce.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Push.transform.localPosition = new Vector3 (-0.2f,0,0);
			infoPanel.SetActive (false);
			Activity_Instpanel.SetActive (false);
			ActivateTypesOfforces_Btn (true);
			isTypesOfforceActivity = false;
		}


	}

	void DisableAllBtns(){
		homeBtn.SetActive (false);
		DisableInfoUI ();
		pushBtn.SetActive (false);
		pullBtn.SetActive (false);
		gravitationalForce_Btn.SetActive (false);
		magneticForce_Btn.SetActive (false);
		frictionForce_Btn.SetActive (false);
		muscularForce_Btn.SetActive (false);
		Activity_Instpanel.SetActive (false);
		infoPanel.SetActive (false);
	}
	#region PushActivity

	public void ClickOn_PushBtn() {
	
		isForceActivity = true;
		pushBtn.SetActive (false);
		pullBtn.SetActive (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Push.SetActive (true);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().stopBoy = false;
		homeBtn.SetActive (false);
		Activity_Instpanel.SetActive (true);
		forcesActivity_Txt.text = Types_of_Forces_TextManager.instance.inst_PushActivity;
		Invoke ("AfterPushActivity",_currModel.GetComponent<Types_of_Forces_Objectmanager> ().pushClip.length);
	}

	void AfterPushActivity(){
		if (isModelDetected) {
//			pushBtn.SetActive (true);
//			pullBtn.SetActive (true);
//			homeBtn.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Push.SetActive (false);
//			Activity_Instpanel.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().stopBoy=true;
			homeBtn.SetActive(true);
		}
	}

	#endregion

	#region PullActivity

	public void ClickOn_PullBtn(){
		isForceActivity = true;
		pushBtn.SetActive (false);
		pullBtn.SetActive (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Pull.SetActive (true);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().stopBoy=false;
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Pull.GetComponent<Animator> ().speed = 1;
		homeBtn.SetActive (false);
		Activity_Instpanel.SetActive (true);
		forcesActivity_Txt.text = Types_of_Forces_TextManager.instance.inst_PullActivity;
		Invoke ("AfterPullActivity",_currModel.GetComponent<Types_of_Forces_Objectmanager> ().pullClip.length*2.5f);
	}

	void AfterPullActivity(){
		if (isModelDetected) {
//			pushBtn.SetActive (true);
//			pullBtn.SetActive (true);
//			homeBtn.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Pull.SetActive (false);
//			Activity_Instpanel.SetActive (false);
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Pull.GetComponent<Animator> ().speed = 0;
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().stopBoy=true;
			homeBtn.SetActive(true);
		}
	}
	#endregion

	public void ClickOn_TypeOfForcesBtn() {
		ActivateStartBtn (false);
		homeBtn.SetActive (true);
		ActivateTypesOfforces_Btn (true);
	}

	void ActivateTypesOfforces_Btn(bool status) {
		gravitationalForce_Btn.SetActive (status);
		frictionForce_Btn.SetActive (status);
		muscularForce_Btn.SetActive (status);
		magneticForce_Btn.SetActive (status);
	}

	#region GravitationalForce Activity

	public void ClickOn_GravitationalBtn(){
		isTypesOfforceActivity = true;
		ActivateTypesOfforces_Btn (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_GravitationalForce.SetActive (true);
		homeBtn.SetActive (false);
		infoPanelTxt.text = Types_of_Forces_TextManager.instance.inst_GravitationalForce;
		infoPanel.SetActive (true);
		Activity_Instpanel.SetActive (true);
		forcesActivity_Txt.text = Types_of_Forces_TextManager.instance.inst_GravityActivity;
		Invoke ("AfterGravitationalActivity",_currModel.GetComponent<Types_of_Forces_Objectmanager> ().gravityClip.length);
	}

	void AfterGravitationalActivity(){
		if (isModelDetected) {
//			ActivateTypesOfforces_Btn (true);
//			homeBtn.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_GravitationalForce.SetActive (false);
//			Activity_Instpanel.SetActive (false);
//			DisableInfoUI ();
			homeBtn.SetActive(true);
		}
	}
	#endregion

	#region FrictionForce Activity

	public void ClickOn_FrictionForceBtn(){
		isTypesOfforceActivity = true;
		ActivateTypesOfforces_Btn (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Push.SetActive (true);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().frictionArrow.SetActive (true);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().stopBoy=false;
		homeBtn.SetActive (false);
		infoPanelTxt.text = Types_of_Forces_TextManager.instance.inst_FrictionalForce;
		infoPanel.SetActive (true);
		Activity_Instpanel.SetActive (true);
		forcesActivity_Txt.text = Types_of_Forces_TextManager.instance.inst_FrictionActivity;
		Invoke ("AfterFrictionForceActivity",_currModel.GetComponent<Types_of_Forces_Objectmanager> ().pushClip.length+0.5f);
	}

	void AfterFrictionForceActivity(){
		if (isModelDetected) {
//			ActivateTypesOfforces_Btn (true);
//			homeBtn.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Push.SetActive (false);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().frictionArrow.SetActive (false);
//			Activity_Instpanel.SetActive (false);
//			DisableInfoUI ();
			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().stopBoy=true;
			homeBtn.SetActive(true);
		}
	}
	#endregion

	#region MuscularForce Activity

	public void ClickOn_MuscularForceBtn(){
		isTypesOfforceActivity = true;
		ActivateTypesOfforces_Btn (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_MuscularForce.SetActive (true);
		homeBtn.SetActive (false);
		infoPanelTxt.text = Types_of_Forces_TextManager.instance.inst_MusculurForce;
		infoPanel.SetActive (true);
		Activity_Instpanel.SetActive (true);
		forcesActivity_Txt.text = Types_of_Forces_TextManager.instance.inst_MusculurActivity;
		Invoke ("AfterMuscularForceActivity",_currModel.GetComponent<Types_of_Forces_Objectmanager> ().muscularClip.length*2);
	}

	void AfterMuscularForceActivity(){
		if (isModelDetected) {
//			ActivateTypesOfforces_Btn (true);
//			homeBtn.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_MuscularForce.SetActive (false);
//			Activity_Instpanel.SetActive (false);
//			DisableInfoUI ();
			homeBtn.SetActive(true);
		}
	}
	#endregion

	#region MagneticForce Activity

	public void ClickOn_MagneticForceBtn(){
		isTypesOfforceActivity = true;
		ActivateTypesOfforces_Btn (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (false);
		_currModel.GetComponent<Types_of_Forces_Objectmanager> ().magneticeForce.SetActive (true);
		homeBtn.SetActive (false);
		infoPanelTxt.text = Types_of_Forces_TextManager.instance.inst_MagneticForce;
		infoPanel.SetActive (true);
		Activity_Instpanel.SetActive (true);
		forcesActivity_Txt.text = Types_of_Forces_TextManager.instance.inst_MagneticActivity;
		Invoke ("AfterMagneticForceActivity",_currModel.GetComponent<Types_of_Forces_Objectmanager> ().magneticClip.length);
	}

	void AfterMagneticForceActivity(){
		if (isModelDetected) {
//			ActivateTypesOfforces_Btn (true);
//			homeBtn.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().boy_Idle.SetActive (true);
//			_currModel.GetComponent<Types_of_Forces_Objectmanager> ().magneticeForce.SetActive (false);
//			Activity_Instpanel.SetActive (false);
//			DisableInfoUI ();
			homeBtn.SetActive(true);
		}
	}
	#endregion

	void DisableInfoUI(){
		isInfoEnable = false;
		infoPanel.transform.GetComponent<Animator> ().SetBool ("infoEnable", false);
		infoPanel.transform.GetComponent<Animator> ().SetBool ("infoDisable", false);
		infoPanel.transform.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0,-95);
		infoPanelCloseBtn.SetActive (false);
		infoPanel.SetActive (false);
	}

	public void EnableInfoPanel(){
		isInfoEnable = !isInfoEnable;
		if (isInfoEnable) {
			infoPanelCloseBtn.SetActive (true);
			infoPanel.transform.GetComponent<Animator> ().SetBool ("infoEnable", true);
			infoPanel.transform.GetComponent<Animator> ().SetBool ("infoDisable", false);
		} else {
			infoPanelCloseBtn.SetActive (false);
			infoPanel.transform.GetComponent<Animator> ().SetBool ("infoEnable", false);
			infoPanel.transform.GetComponent<Animator> ().SetBool ("infoDisable", true);
		}
	}

	public void ScreenShotUImanager(){
		StartCoroutine (ManageUIPanels());
	}

	IEnumerator ManageUIPanels(){
		uiPanels.SetActive (false);
		wizarLogo.SetActive (true);
		yield return new WaitForSeconds (3.5f);
		uiPanels.SetActive (true);
		wizarLogo.SetActive (false);
	}
}
