﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Types_of_Forces_ZoomInOut : MonoBehaviour {

    public static Types_of_Forces_ZoomInOut instance;

    public float rotateSpeed,zoomSpeed;
    public float minZoom, maxZoom;

    internal bool isModlDetected;
	internal GameObject currModel;

    // Use this for initialization
    void Awake () {
		currModel = new GameObject ();
        instance = this;
	}

    //Update is called once per frame

    void Update()
    {
		if (!isModlDetected) {
			return;
		}
        
        if (Input.touchCount == 2 )
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            if (currModel!=null) {
                currModel.transform.localScale = new Vector3(currModel.transform.localScale.x - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime,
                currModel.transform.localScale.y - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime,
                currModel.transform.localScale.z - deltaMagnitudeDiff * zoomSpeed * Time.deltaTime);
            }

        }
        else if (Input.touchCount == 1 )
        {
            Touch touchZero = Input.GetTouch(0);

            if (touchZero.phase == TouchPhase.Moved)
            {
                float swipeSpeed = touchZero.deltaPosition.x;

				if (currModel != null) {
                    currModel.transform.Rotate(new Vector3(0, -rotateSpeed * swipeSpeed, 0));
                 }
            }

        }

           if (currModel != null)
           {
               if (currModel.transform.localScale.x < minZoom)
               {
                    currModel.transform.localScale = new Vector3(minZoom, minZoom, minZoom);
               }
               else if (currModel.transform.localScale.x > maxZoom)
               {
                    currModel.transform.localScale = new Vector3(maxZoom, maxZoom, maxZoom);
               }
           }
       
    }
}
