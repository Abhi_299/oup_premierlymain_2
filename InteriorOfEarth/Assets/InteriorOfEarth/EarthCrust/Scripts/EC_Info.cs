﻿using UnityEngine;
using System.Collections;

public class EC_Info : MonoBehaviour {

	public static string[,] info = new string[,] {
		{"Line_Crust_up","The Earth crust ranges from 5–70 km (~3–44 miles) in depth and is the outermost layer. The thin parts are the oceanic crust, which underlie the ocean basins (5–10 km) and thicker crust is continental crust."},
		{"Line_inner_core","The Earth's inner core is the Earth's innermost part and it has been believed to be primarily a solid ball with a radius of about 1220 kilometers. It is composed of an iron–nickel alloy and some light elements. The temperature at the inner core boundary is approximately 5700 Kelvin."},
		{"Line_Outer_core","The outer core of the Earth is a fluid layer about 2,300 km thick and composed of mostly iron and nickel that lies above Earth's solid inner core and below its mantle. Its outer boundary lies 2,890 km beneath Earth's surface. The transition between the inner core and outer core is located approximately 5,150 km beneath the Earth's surface."},
		{"Line_Core_Mantle_Boundary","The core–mantle boundary of the Earth lies between the planet's mantle and its liquid outer core. This boundary is located at approximately 2891 km depth beneath the Earth's surface."},
		{"Line_Mantle","The Mantle is a layer inside a terrestrial planet (In this case Earth) and some other rocky planetary bodies. The mantle lies between the core below and the crust above. The mantle differs substantially from the crust in its mechanical properties which is the direct consequence of chemical composition change (expressed as different mineralogy)."},
		{"Line_Atmosphere","An Atmosphere, meaning \"vapour\", and \"sphere\" is a layer of gases surrounding a planet (In this case, Earth) or other material body, that is held in place by the gravity of that body. An atmosphere is more likely to be retained if its gravity is high and the atmosphere's temperature is low."},
		{"Line_Lithosphere","A lithosphere is the rigid, outermost shell of a terrestrial-type planet (in this case Earth) that is defined by its rigid mechanical properties. On Earth, it is composed of the crust and the portion of the upper mantle that behaves elastically on time scales of thousands of years or greater."}
	};

	public static string[] heading = new string[] {
		"Crust",
		"Inner Core",
		"Outer Core",
		"Core Mantle Boundary",
		"Mantle",
		"Atmosphere",
		"Lithosphere"
	};
	

}
