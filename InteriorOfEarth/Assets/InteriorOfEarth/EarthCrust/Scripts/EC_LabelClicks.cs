﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EC_LabelClicks : MonoBehaviour {

	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnLableClicked (string arg)
	{
		Debug.Log(arg);
		for (int i = 0; i < EC_Info.info.Length / 2 && arg != null; i++) {
			if (arg.Trim () == EC_Info.info [i, 0]) {
				EarthScriptController.instance.infoHeading.GetComponent<Text> ().text = "" + EC_Info.heading [i];
				EarthScriptController.instance.infoDes.GetComponent<Text> ().text = "" + EC_Info.info [i, 1];
			}
		}
		EarthScriptController.instance.openInfo ();	
		Debug.Log(arg);
	}
}
