﻿using UnityEngine;
using System.Collections;

public class EC_InputHandler : MonoBehaviour
{
    private const int rotateSpeed = 20;
    [HideInInspector]
	public GameObject target;
	[HideInInspector]
	public bool isOpen;
	public bool backRotation;
	float touchDuration;

	Touch touch;

	private Vector3 theSpeed;
	private Vector3 avgSpeed;
	private bool isDragging = false;
	private Vector3 targetSpeedX;
	private GameObject parentObject;


	void OnEnable() {
		LoadModelOnDetection.isModelLoaded += OnModel_Detection;
	}

	void OnDisable() {
		LoadModelOnDetection.isModelLoaded -= OnModel_Detection;
	}

	public void OnModel_Detection(string name,Transform _Transform,bool status,LoadModelOnDetection.SceneType sceneType){
		if (!status) {
			isOpen = false;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.touchCount == 1) {
			foreach (Touch touch in Input.touches) {
				if (touch.phase == TouchPhase.Began) {	
					Ray ray = Camera.main.ScreenPointToRay (touch.position);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
						target = hit.transform.gameObject;
						parentObject = target;
						if (parentObject.transform.parent.name.ToLower()=="interior_of_earth") {
							target.GetComponentInChildren<EC_RotateAround> ().enabled = false;
							EarthScriptController.instance.ShouldEnableLabels (false);
							isDragging = true;
						}
						
					}
				}
				if (target != null && touch.phase == TouchPhase.Moved && isDragging &&  target.gameObject.name=="Earth") {
					target.transform.Rotate (touch.deltaPosition.y * Time.deltaTime * rotateSpeed, -touch.deltaPosition.x * Time.deltaTime * rotateSpeed, 0, Space.World);
				}
				if (target != null && backRotation && touch.phase == TouchPhase.Ended &&  target.gameObject.name=="Earth") {
					if (isDragging) {
						iTween.RotateTo (target, new Vector3 (0, target.transform.localRotation.y, 0), 0.5f);
						Invoke ("EnableScript", 0.5f);
						isDragging = false;
						parentObject = null;
					} 
				}
			}

		}
		if (Input.touchCount > 0) { //if there is any touch
			touchDuration += Time.deltaTime;
			touch = Input.GetTouch (0);

			if (touch.phase == TouchPhase.Ended && touchDuration < 0.5f) {
				//making sure it only check the touch once && it was a short touch/tap and not a dragging.	
				Ray ray = Camera.main.ScreenPointToRay (touch.position);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
					isDragging = true;
					target = hit.transform.gameObject;
					parentObject = target;
					Debug.Log ("target " + target.name);
					if(parentObject.gameObject.name!="Label")
                        StartCoroutine ("singleOrDouble");
				}
			}

		} else {
			touchDuration = 0.0f;
		}

		if (target != null) {
			target.GetComponentInChildren<EC_RotateAround> ().enabled = !isOpen;
		}

	}

	IEnumerator singleOrDouble ()
	{
		yield return new WaitForSeconds (0.3f);
		if (touch.tapCount == 1) {
			// Single Tape

		} else if (touch.tapCount == 2) {
			//this coroutine has been called twice. We should stop the next one here otherwise we get two double tap
			StopCoroutine ("singleOrDouble");
			// Double tap
			DisectEarth ();
		}
	}

	void EnableScript ()
	{
		if (parentObject != null)
			EarthScriptController.instance.ShouldEnableLabels (true);
		if (!isOpen) {
			target.GetComponentInChildren<EC_RotateAround> ().enabled = true;
		}
	}

	void DisectEarth ()
	{
		if (parentObject != null && !EarthScriptController.instance.getHUDStatus ()) {
			isOpen = target.GetComponent<Animator> ().GetBool ("isOpen");
			isOpen = !isOpen;
			Debug.Log ("isOpen " + isOpen);
			target.GetComponent<Animator> ().SetBool ("isOpen", isOpen);
			EarthScriptController.instance.ShouldEnableLabels (isOpen);
			if (isOpen) {
				target.GetComponent<CapsuleCollider> ().height = 4f;
			} else {
				target.GetComponent<CapsuleCollider> ().height = 2.5f;
				target.GetComponent<EC_RotateAround> ().enabled = true;
			}
		}
	}
}
