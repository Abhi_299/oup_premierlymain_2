﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EarthScriptController : MonoBehaviour
{
    private  GameObject label;

	public GameObject parentCanvas, inputHandler, searchingImage;

	public GameObject pnl_intro;
	public GameObject pnl_help;
	public GameObject pnl_Info;
	public GameObject pnl_NoImageTarget;

	

	public GameObject infoDes;
	public GameObject infoHeading;

	public AudioClip buttonClick;
	
	public static EarthScriptController instance;

	public GameObject wizarLogo,backBtn,screenShotBtn;

	enum WHAT_IS_SELECTED
	{
		HELP,
		INTRO,
		INFO,
		NO_IMAGE_TARGET}

	;

	bool isInfoOpen;
	bool isHelpOpen;
	bool isIntroOpen;

	private void Start()
	{
		instance = this;
	}
	

	public void opnIntro ()
	{
		Debug.Log(isIntroOpen);
		isIntroOpen = !isIntroOpen;
		if (isIntroOpen) {
			iTween.ScaleTo (pnl_intro, new Vector3 (1, 1, 1), 0.5f);
//			if(Earth.GetComponent<Animator> ().GetBool ("isOpen")) {
//				Earth.GetComponent<Animator>().SetBool("isOpen",false);
//			}

			ShouldEnableLabels (false);
		} else {
			iTween.ScaleTo (pnl_intro, new Vector3 (0, 0, 0), 0.5f);
			ShouldEnableLabels (true);
		}
		disableOther (WHAT_IS_SELECTED.INTRO);
	}

	public void openHelp ()
	{
		Debug.Log(isHelpOpen);
		isHelpOpen = !isHelpOpen;
		if (isHelpOpen) {
			iTween.ScaleTo (pnl_help, new Vector3 (1, 1, 1), 0.5f);
//			if(Earth.GetComponent<Animator> ().GetBool ("isOpen")) {
//				Earth.GetComponent<Animator>().SetBool("isOpen",false);
//			}
			ShouldEnableLabels (false);
		} else {
			iTween.ScaleTo (pnl_help, new Vector3 (0, 0, 0), 0.5f);
			ShouldEnableLabels (true);
		}
		disableOther (WHAT_IS_SELECTED.HELP);
	}

	public  void openInfo ()
	{
		Debug.Log(isInfoOpen);
		isInfoOpen = !isInfoOpen;
		if (isInfoOpen) {
			iTween.ScaleTo (pnl_Info, new Vector3 (1, 1, 1), 0.5f);
			if (inputHandler.GetComponent <EC_InputHandler> ().isOpen) {
				ShouldEnableLabels (false);
			}
		} else {
			iTween.ScaleTo (pnl_Info, new Vector3 (0, 0, 0), 0.5f);
			if (inputHandler.GetComponent<EC_InputHandler> ().isOpen) {
				ShouldEnableLabels (true);
			}
		}
		disableOther (WHAT_IS_SELECTED.INFO);
	}


	// To disable others buttons
	void disableOther (WHAT_IS_SELECTED arg)
	{
		if (arg == WHAT_IS_SELECTED.HELP) {
			isIntroOpen = false;
			isInfoOpen = false;

			iTween.ScaleTo (pnl_intro, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (pnl_Info, new Vector3 (0, 0, 0), 0.5f);
		} else if (arg == WHAT_IS_SELECTED.INTRO) {
			isHelpOpen = false;
			isInfoOpen = false;

			iTween.ScaleTo (pnl_help, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (pnl_Info, new Vector3 (0, 0, 0), 0.5f);
		} else if (arg == WHAT_IS_SELECTED.INFO) {
			isHelpOpen = false;
			isIntroOpen = false;

			iTween.ScaleTo (pnl_help, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (pnl_intro, new Vector3 (0, 0, 0), 0.5f);
		}
		playbuttonClick ();
	}

	public void ShouldEnableLabels (bool arg)
	{
		Debug.Log ("arg value " + arg);
		Debug.Log ("arg value " + label);
		if (arg) {
			
			if (inputHandler.GetComponent<EC_InputHandler> ().isOpen) {
				label.SetActive (true);
			}
		} else if (!arg) {
			label.SetActive (false);
		}
	}

	// To check whether intro or help panel are already open
	public bool getHUDStatus ()
	{
		return (isHelpOpen || isIntroOpen || isInfoOpen);
	}

	

	void OnEnable ()
	{
		LoadModelOnDetection.isModelLoaded  += onImageTargetDetected;
	}


	void OnDisable ()
	{
		LoadModelOnDetection.isModelLoaded  -= onImageTargetDetected;
	}

	void onImageTargetDetected (string name, Transform _transform, bool status, LoadModelOnDetection.SceneType  sceneType)
	{
		if (status)
		{
			label = _transform.GetChild(0).Find("CanvasWorldCanvas").gameObject;
			Debug.Log(label.name);
			if (pnl_NoImageTarget != null)
				iTween.ScaleTo (pnl_NoImageTarget, new Vector3 (0, 0, 0), 0.25f);
			if(parentCanvas!=null)
		      	parentCanvas.SetActive (true);
//			if (label != null && !isInfoOpen && !isHelpOpen && !isIntroOpen)
//				ShouldEnableLabels (true);
			searchingImage.SetActive(false);
		}
		if (!status) {
			if (pnl_NoImageTarget != null)
				iTween.ScaleTo (pnl_NoImageTarget, new Vector3 (1, 1, 1), 0.25f);
			if (parentCanvas != null)
				parentCanvas.SetActive (false);
		//	if (label != null)
		//		ShouldEnableLabels (false);
		      searchingImage.SetActive(true);
		}
	}

	void playbuttonClick ()
	{
		GetComponent<AudioSource> ().PlayOneShot (buttonClick);
	}

	public void ManageScreenShotUI() {
		StartCoroutine (ScreenShotUiManager());
	}

	IEnumerator ScreenShotUiManager(){

		parentCanvas.SetActive (false);
		wizarLogo.SetActive (true);
		backBtn.SetActive (false);
		screenShotBtn.SetActive (false);
		yield return new WaitForSeconds (3.5f);
		parentCanvas.SetActive (true);
		wizarLogo.SetActive (false);
		backBtn.SetActive (true);
		screenShotBtn.SetActive (true);
	}
}
