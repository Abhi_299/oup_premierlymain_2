﻿using UnityEngine;
using System.Collections;

public class EC_RotateAround : MonoBehaviour {

	public Transform target;
	public float speed = 2;

	void Start () {
	}

	void Update () {
		transform.RotateAround(target.transform.position, Vector3.up, -speed*20 * Time.deltaTime);
	}
}
