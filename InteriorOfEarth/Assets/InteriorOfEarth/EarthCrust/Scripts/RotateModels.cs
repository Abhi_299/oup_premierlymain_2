﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateModels : MonoBehaviour
{
	#region variable

	private bool isDragging;
	private GameObject target;
	private Transform preserved;

	#endregion

	#region public varaibles

	public bool onlyYAxis, backRotation;
	public GameObject rotateModel;

	#endregion

	// Update is called once per frame
	void Update ()
	{
       // Debug.Log("update called " +Input.touchCount);
		if (Input.touchCount == 1) {
			//Debug.Log ("on touch first time");
			foreach (Touch touch in Input.touches) {
				//Debug.Log ("on touch second frame");
				if (touch.phase == TouchPhase.Began) {													
					Ray ray = Camera.main.ScreenPointToRay (touch.position);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
					//	Debug.Log ("raycast not hit");
						isDragging = true;
						target = hit.transform.gameObject;
						preserved = hit.transform;
					} else if (rotateModel != null) {
                        Debug.Log("else true");
						isDragging = true;
						target = rotateModel.transform.gameObject;
						preserved = rotateModel.transform;
					}
				}
              //  if(target!=null)
              //       Debug.Log(target.gameObject.CompareTag("RotateMe"));
				if (target !=null && touch.phase == TouchPhase.Moved && isDragging && target.gameObject.gameObject.name=="Earth") {
                    
					if (onlyYAxis)
						target.transform.Rotate (target.transform.rotation.x, -touch.deltaPosition.x * Time.deltaTime * 20, 0, Space.World);
					else
						target.transform.Rotate (touch.deltaPosition.y * Time.deltaTime * 20, -touch.deltaPosition.x * Time.deltaTime * 20, 0, Space.World);
					
					}
				if (target != null && backRotation && touch.phase == TouchPhase.Ended && target.gameObject.gameObject.name=="Earth") {
					if (isDragging) {
						isDragging = false;
							iTween.RotateTo (target, new Vector3 (preserved.transform.rotation.x, preserved.transform.rotation.y, preserved.transform.rotation.z), 0.5f);
					} 
				}
			}
		}
	}

}
