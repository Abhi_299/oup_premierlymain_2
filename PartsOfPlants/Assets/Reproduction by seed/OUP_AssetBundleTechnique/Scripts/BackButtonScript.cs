﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButtonScript : MonoBehaviour
{
	public string bundleIdentifier= "com.excelSoft.oupi.myBag";
	public static BackButtonScript instance;

	private void Start()
	{
		instance = this;
	}

	public void BackBtn()
	{
       SceneManager.LoadScene("menu");

    }


    public void MyBagOpen(string identifer)
    {
        bundleIdentifier = identifer;
        BackBtn();
    }

}
