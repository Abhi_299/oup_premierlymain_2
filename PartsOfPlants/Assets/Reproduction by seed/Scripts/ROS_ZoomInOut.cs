﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ROS_ZoomInOut : MonoBehaviour {

	#region private variables

	private Touch touch;
	private float rotationAngles=0;
	#endregion

	#region public variables.

	public float minScale, maxScale;

	#endregion

	private void Start()
	{
		rotationAngles = this.transform.eulerAngles.y;
	}

	// Update is called once per frame
	void Update () {

		if (!IsPointerOverUIObject())
		{
			if (Input.touchCount == 1)
			{
				touch = Input.GetTouch(0);
				if (touch.phase == TouchPhase.Moved)
				{
					RotateObject();
				}
			}

			if (Input.touchCount == 2 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved))
			{
				// Store both touches. 
				Touch touchZero = Input.GetTouch(0);
				Touch touchOne = Input.GetTouch(1);
				// Find the position in the previous frame of each touch.
				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
				// Find the magnitude of the vector (the distance) between the touches in each frame. 
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
				// Find the difference in the distances between each frame. 
				float deltaMagnitudeDiff = touchDeltaMag - prevTouchDeltaMag;
				this.transform.localScale = new Vector3(Mathf.Clamp(this.transform.localScale.x + deltaMagnitudeDiff * 0.0008f, minScale, maxScale + 0.04f),
															 Mathf.Clamp(this.transform.localScale.y + deltaMagnitudeDiff * 0.0008f, minScale, maxScale + 0.04f),
															 Mathf.Clamp(this.transform.localScale.z + deltaMagnitudeDiff * 0.0008f, minScale, maxScale + 0.04f));
			}
		}
	}

	// ROTATE THE GAMEOBJECT WHICH IS DETECTED FROM MARKER USING TOUCH
	public void RotateObject()
	{
		if (this.transform.childCount!=0)
		{
			try
			{
				if (this.gameObject != null)
				{
					rotationAngles += ((touch.deltaPosition.x / 4f));
					Debug.Log("rotation "+rotationAngles);
					this.transform.localEulerAngles = new Vector3(this.transform.localEulerAngles.x, -(rotationAngles),
						this.transform.localEulerAngles.z);
					//this.GetComponentsInChildren<Transform>()[1].eulerAngles = new Vector3(this.GetComponentsInChildren<Transform>()[1].eulerAngles.x, (rotationAngles+this.GetComponentsInChildren<Transform>()[1].eulerAngles.y ),
					//		this.GetComponentsInChildren<Transform>()[1].eulerAngles.z);
				}
			}
			catch (UnityException e)
			{
				Debug.Log("" + e.ToString());
			}
		}
	}

	private static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp(angle, min, max);
	}

	// To avoid touch on button while moving the target
	private bool IsPointerOverUIObject()
	{
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}
}
