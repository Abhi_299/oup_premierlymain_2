﻿using System.Collections;
using System.IO;
using UnityEngine;



public class GallarySaver : MonoBehaviour
{
	public static GallarySaver gallerySaver;
	CaptureAndSave snapShot;

	void Start(){
		if (gallerySaver == null)
		{
			DontDestroyOnLoad(this);
			gallerySaver = this;
            snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
		}
		else Destroy(this);
	}	
#if !UNITY_EDITOR && UNITY_ANDROID
    private static AndroidJavaClass m_ajc = null;
    private static AndroidJavaClass AJC
    {
        get
        {
            if( m_ajc == null )
                m_ajc = new AndroidJavaClass( "com.simsoft.sshelper.SSHelper" );
 
            return m_ajc;
        }
    }
#endif
 
	public void SavePictureToAndroidGallery(byte[] bytes, string filename)
	{
        if (!Directory.Exists(GetPicturesFolderPath()))
            Directory.CreateDirectory(GetPicturesFolderPath());
        string path = Path.Combine(GetPicturesFolderPath(), filename);
        File.WriteAllBytes(path, bytes);

        #if !UNITY_EDITOR && UNITY_ANDROID
                AndroidJavaObject context;
                using( AndroidJavaClass unityClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ) )
                {
                    context = unityClass.GetStatic<AndroidJavaObject>( "currentActivity" );
                }
         
                AJC.CallStatic( "MediaScanFile", context, path );
        #endif
    }

	public void SavePictureToIOSGallery(Texture2D texture, string filename){
		snapShot.SaveTextureToGallery(texture);
		if (!Directory.Exists(GetPicturesFolderPath()))
            Directory.CreateDirectory(GetPicturesFolderPath());
        string path = Path.Combine(GetPicturesFolderPath(), filename);
		File.WriteAllBytes(path, texture.EncodeToPNG());
	}


	public void DeletePictureFromPersistentDataPath(string filename)
    {
		if (!Directory.Exists(GetPicturesFolderPath()))
			Directory.CreateDirectory(GetPicturesFolderPath());
        string path = Path.Combine(GetPicturesFolderPath(), filename);
		if (File.Exists(path))
			File.Delete(path);
    }

	public string GetPicturesFolderPath()
	{
		
#if UNITY_EDITOR
		return System.Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory);
#elif UNITY_ANDROID
        return AJC.CallStatic<string>( "GetPicturesFolderPath", Application.productName );
#else
        return Application.persistentDataPath;
#endif
	}


}