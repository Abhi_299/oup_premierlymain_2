﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ReproducationOFSeeds
{
	public class ROS_DetectedObjectAssets : MonoBehaviour
	{
       
		public GameObject seed;
		public GameObject labelCanvas;
		public GameObject plantGrowth;
		public GameObject sun;
		public GameObject waterCane;
//		public GameObject textGermination;
		

		public static ROS_DetectedObjectAssets instance;
		private void Awake()
		{
			instance = this;
		}

		public void openDescription(string name)
		{
			ROS_SeedsController.instance.OpenInfo(name);
		}
	}
}
