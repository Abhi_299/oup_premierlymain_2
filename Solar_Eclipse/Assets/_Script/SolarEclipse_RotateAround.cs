﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarEclipse_RotateAround : MonoBehaviour
{

    public Transform target;
    public float OrbitDegrees = 1f;

    void Update()
    {
        if (!Solar_eclipse.instance.path)
        {
            transform.RotateAround(target.position, Vector3.up, OrbitDegrees);
        }
    }
}