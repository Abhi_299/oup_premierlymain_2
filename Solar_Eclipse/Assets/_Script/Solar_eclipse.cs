﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Solar_eclipse : MonoBehaviour {

    public static Solar_eclipse instance;

    public List<GameObject> LoadedModel;
    public List<string> Description;
    public GameObject infoBtn;
    public GameObject ARCam;
    public bool path;
    public bool StartAnim;

    void Start()
    {
        instance = this;
    }

    private void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += IsModelLoaded;
    }

    private void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= IsModelLoaded;
    }

    public void IsModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
    {
        Debug.Log("SolarEclipse:"+ StartAnim);
        Debug.Log("SolarEclipse:" + name);

            SolarEclipse_ZoomInOut.instance.isModlDetected = status;

            if (status)
            {
                SolarEclipse_UIHandler.instance.SearchCanvas.SetActive(false);
                SolarEclipse_UIHandler.instance.SolarEclipse_Btn.SetActive(true);
                if (name == "solar_eclipse")
                {
                    Debug.Log(_transform.gameObject.transform.GetChildCount() + "total");
                    if (_transform.gameObject.transform.GetChildCount() > 0)
                    {
                        Debug.Log("detected");
                        for (int i = 0; i < _transform.gameObject.transform.GetChild(0).transform.GetChildCount(); i++)
                        {
                            LoadedModel.Add(_transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.gameObject);
                        }
                    }

                    Debug.Log(_transform.gameObject);
                    SolarEclipse_ZoomInOut.instance.currModel = _transform.gameObject.transform.GetChild(0).gameObject;
                }
            }
            else
            {
                LoadedModel.Clear();
                path = false;
                SolarEclipse_UIHandler.instance.Home_Btn.SetActive(false);
                SolarEclipse_UIHandler.instance.SolarEclipse_Btn.SetActive(false);
                SolarEclipse_UIHandler.instance.UmberaRegion.SetActive(false);
                SolarEclipse_UIHandler.instance.AnnularEclipse.SetActive(false);
                SolarEclipse_UIHandler.instance.PenumbraRegion.SetActive(false);
                SolarEclipse_UIHandler.instance.SearchCanvas.SetActive(true);
                SolarEclipse_UIHandler.instance.AnnularEclipse_Image.SetActive(false);
                SolarEclipse_UIHandler.instance.TotalEclipse_Image.SetActive(false);
            infoBtn.SetActive(false);
            }
    }


    public void SolarEclipse()
    {
        Debug.Log("SolarEclipse");

        SphereCollider[] colliderComponents = LoadedModel[1].GetComponentsInChildren<SphereCollider>(false);

        foreach (SphereCollider component in colliderComponents)
        {
            Debug.Log("collider");
            component.enabled = true;
        }
    }

    public void infoPannel()
    {
        infoBtn.SetActive(true);
        infoBtn.GetComponent<Animator>().enabled = true;

        if (!infoBtn.GetComponent<Animator>().GetBool("EnableInfo"))
        {
            infoBtn.GetComponent<Animator>().SetBool("EnableInfo", true);
            infoBtn.GetComponent<Animator>().Play("popup_info");
        }
        else
        {
            infoBtn.GetComponent<Animator>().SetBool("EnableInfo", false);
            infoBtn.GetComponent<Animator>().Play("popup_info 0");
        }
       
    }

    public void Cross()
    {
        infoBtn.GetComponent<Animator>().enabled = true;
        infoBtn.GetComponent<Animator>().SetBool("EnableInfo", false);
        infoBtn.GetComponent<Animator>().Play("popup_info 0");
    }

    public void HomeBtn()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
