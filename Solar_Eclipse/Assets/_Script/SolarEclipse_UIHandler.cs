﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolarEclipse_UIHandler : MonoBehaviour {

    public static SolarEclipse_UIHandler instance;

    public GameObject Home_Btn;
    public GameObject SolarEclipse_Btn;
    public GameObject UmberaRegion;
    public GameObject PenumbraRegion;
    public GameObject AnnularEclipse;
    public GameObject AnnularEclipse_Image;
    public GameObject TotalEclipse_Image;
    public Text DescriptionText;
    public GameObject SearchCanvas;

    // Use this for initialization
    void Awake ()
    {
        instance = this;
        //searching_Icon.SetActive(true);
    }
}
