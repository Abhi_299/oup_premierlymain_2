﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoScrollDown : MonoBehaviour {

//public Text test;
    public List<ScrollRect> scrollViewer;
    public float decrementalValue;

    ScrollRect enabledRect;
    float pos=1;
    private bool touched;

    void OnEnable()
    {
        for(int i=0; i<scrollViewer.Count;i++)
        {
            if(scrollViewer[i].gameObject.activeSelf)
            {
                enabledRect = scrollViewer[i];
            }
        }
        enabledRect.verticalNormalizedPosition = 1;
    }
	
    void FixedUpdate()
    {
        //test.gameObject.SetActive(true);
        if(!enabledRect.gameObject.activeInHierarchy)
        {
            for (int i = 0; i < scrollViewer.Count; i++)
            {
                if (scrollViewer[i].gameObject.activeInHierarchy)
                {
                    enabledRect = scrollViewer[i];
                }
            }
            enabledRect.verticalNormalizedPosition = 1;
        }
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
            touched = true;
        else
            touched = false;
#else
        if(Input.touchCount >=1)
        {
        touched = true;
        
        }
        else 
        {
        touched = false;
        
        }
       
#endif

        if (!touched )
        {
            pos -= decrementalValue * Time.deltaTime;
            enabledRect.verticalNormalizedPosition = pos;
            if (pos <= 0) pos = 1;
        }
        else
        {
            pos = enabledRect.verticalNormalizedPosition;
        }
    }

}

