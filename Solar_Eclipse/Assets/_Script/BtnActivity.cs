﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;

public class BtnActivity : MonoBehaviour
{
    public GameObject AnnularEclipse;
    public GameObject UmberRegion;
    public GameObject penumbraRegion;
    public GameObject moon;
    public GameObject moonPath;
    public GameObject PurpleRegion;
    public GameObject BlackRegion;
    public GameObject AnnularEclipseRenderer;
    public GameObject EarthTop;
    public GameObject EarthBottom;
    public GameObject EarthTop_;
    public GameObject EarthBottom_;
    public GameObject sunTop;
    public GameObject sunBottom;
    public GameObject CanvasObj;

    public void Umbera()
    {
        Solar_eclipse.instance.infoBtn.SetActive(true);
        SolarEclipse_UIHandler.instance.DescriptionText.text = Solar_eclipse.instance.Description[1].ToString();
        SolarEclipse_UIHandler.instance.UmberaRegion.SetActive(true);
        SolarEclipse_UIHandler.instance.AnnularEclipse.SetActive(false);
        SolarEclipse_UIHandler.instance.PenumbraRegion.SetActive(false);
        SolarEclipse_UIHandler.instance.AnnularEclipse_Image.SetActive(false);
        SolarEclipse_UIHandler.instance.TotalEclipse_Image.SetActive(true);
    }


    public void Penumbra()
    {
        SolarEclipse_UIHandler.instance.AnnularEclipse_Image.SetActive(false);
        SolarEclipse_UIHandler.instance.TotalEclipse_Image.SetActive(true);
        Solar_eclipse.instance.infoBtn.SetActive(true);
        SolarEclipse_UIHandler.instance.DescriptionText.text = Solar_eclipse.instance.Description[3].ToString();
        SolarEclipse_UIHandler.instance.UmberaRegion.SetActive(false);
        SolarEclipse_UIHandler.instance.AnnularEclipse.SetActive(false);
        SolarEclipse_UIHandler.instance.PenumbraRegion.SetActive(true);
        AnnularEclipseRenderer.SetActive(false);
        moon.transform.localPosition= new Vector3(-1.25f, -0.97f, 0.052f);
        PurpleRegion.transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);
        BlackRegion.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
    }

    public void Annular()
    {
        SolarEclipse_UIHandler.instance.AnnularEclipse_Image.SetActive(true);
        SolarEclipse_UIHandler.instance.TotalEclipse_Image.SetActive(false);
        Solar_eclipse.instance.infoBtn.SetActive(true);
        SolarEclipse_UIHandler.instance.DescriptionText.text = Solar_eclipse.instance.Description[2].ToString();
        SolarEclipse_UIHandler.instance.UmberaRegion.SetActive(false);
        SolarEclipse_UIHandler.instance.AnnularEclipse.SetActive(true);
        SolarEclipse_UIHandler.instance.PenumbraRegion.SetActive(false);
        moonPath.GetComponent<MeshRenderer>().enabled = false;
        moon.transform.DOLocalMoveX(-2f, 2f);
        PurpleRegion.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 1f);
        BlackRegion.transform.DOScale(new Vector3(0.05f, 0.05f, 0.05f), 1f);
        StartCoroutine(EnlargeSize());
    }

    public void CloseBtn()
    {
        moon.transform.localPosition = new Vector3(-1.25f, -0.97f, 0.052f);
        PurpleRegion.transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);
        BlackRegion.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
        AnnularEclipse.transform.localPosition = new Vector3(-146.2f, 50.2f,-17.7f);
        SolarEclipse_UIHandler.instance.AnnularEclipse_Image.SetActive(false);
        SolarEclipse_UIHandler.instance.TotalEclipse_Image.SetActive(false);
        SolarEclipse_UIHandler.instance.DescriptionText.text = Solar_eclipse.instance.Description[0].ToString();
        //Solar_eclipse.instance.infoBtn.GetComponent<Animator>().enabled = false;
        //Solar_eclipse.instance.infoBtn.SetActive(false);

        SolarEclipse_UIHandler.instance.UmberaRegion.SetActive(false);
        SolarEclipse_UIHandler.instance.AnnularEclipse_Image.SetActive(false);
        SolarEclipse_UIHandler.instance.TotalEclipse_Image.SetActive(true);
    }

    private IEnumerator EnlargeSize()
    {
        yield return new WaitForSeconds(1f);
        PurpleRegion.transform.DOScale(new Vector3(0.45f, 0.45f, 0.45f), 1f);
        BlackRegion.transform.DOScale(new Vector3(0.15f, 0.15f, 0.15f), 1f);
        AnnularEclipse.transform.DOLocalMoveX(0f, 1f);
        AnnularEclipseRenderer.SetActive(true);
        LineRenderer[] lineRenderer = GetComponentsInChildren<LineRenderer>();
        Debug.Log(lineRenderer.Length);
        lineRenderer[0].SetPosition(0, EarthTop.transform.position);
        lineRenderer[0].SetPosition(1, sunBottom.transform.position);
        lineRenderer[1].SetPosition(0, sunTop.transform.position);
        lineRenderer[1].SetPosition(1, EarthBottom.transform.position);
        lineRenderer[2].SetPosition(0, EarthTop_.transform.position);
        lineRenderer[2].SetPosition(1, sunBottom.transform.position);
        lineRenderer[3].SetPosition(0, EarthBottom_.transform.position);
        lineRenderer[3].SetPosition(1, sunTop.transform.position);
        CanvasObj.SetActive(true);
    }
}
