﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    public GameObject AnimCanvas;
    public Animator Anim;
    float length;


    // Use this for initialization
    void Start()
    {
        Anim.Play("MoonAnimation");
        StartCoroutine(Throw());
	}

    private IEnumerator Throw()
    {
        Debug.Log("enter");
        AnimatorStateInfo info = Anim.GetCurrentAnimatorStateInfo(0);
        length = info.length;
        yield return new WaitForSeconds(length);
        Solar_eclipse.instance.StartAnim = true;
        AnimCanvas.SetActive(false);
    }
}
