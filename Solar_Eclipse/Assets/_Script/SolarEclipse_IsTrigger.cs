﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolarEclipse_IsTrigger : MonoBehaviour {


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("entered");
        Debug.Log(other.name);
        Solar_eclipse.instance.path = true;
        SolarEclipse_ZoomInOut.instance.rotateSpeed = 0f;
        SolarEclipse_ZoomInOut.instance.zoomSpeed = 0f;
        Solar_eclipse.instance.LoadedModel[1].transform.GetChild(0).localPosition = new Vector3(1.34f, 0.018f, 0f);
        SolarEclipse_UIHandler.instance.DescriptionText.text = Solar_eclipse.instance.Description[0].ToString();
        Solar_eclipse.instance.infoBtn.SetActive(true);
        SolarEclipse_UIHandler.instance.TotalEclipse_Image.SetActive(true);

        if (Solar_eclipse.instance.LoadedModel.Count > 0)
        {
            Solar_eclipse.instance.LoadedModel[1].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
            Solar_eclipse.instance.LoadedModel[1].transform.GetChild(0).transform.GetChild(2).GetComponent<SolarEclipse_RotateGalaxy>().enabled = true;
        }

        LineRendering.instance.DrawLine();
        SolarEclipse_UIHandler.instance.Home_Btn.SetActive(true);
    }  
}
