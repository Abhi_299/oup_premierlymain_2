﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarEclipse_LogoScipt : MonoBehaviour {

    public GameObject LeftMenuPanel;
    public GameObject HomeBtn;
    public GameObject btnBack;
    public GameObject wizarLogo;


    public void WizArLogoManager()
    {
        wizarLogo.SetActive(true);
        ActivateUiOnCapture(false);
        Invoke("DisableWizARLogo", 4);
    }

    void DisableWizARLogo()
    {
        wizarLogo.SetActive(false);
        ActivateUiOnCapture(true);
    }

    void ActivateUiOnCapture(bool status)
    {
        btnBack.SetActive(status);
        LeftMenuPanel.SetActive(status);
        HomeBtn.SetActive(status);
    }
}
