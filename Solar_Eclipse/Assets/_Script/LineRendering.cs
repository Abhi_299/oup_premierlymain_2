﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendering : MonoBehaviour {

    public static LineRendering instance;

    void Start()
    {
        instance = this;    
    }

    public void DrawLine()
    {

        if (Solar_eclipse.instance.LoadedModel.Count > 0)
        {
            //Solar_eclipse.instance.LoadedModel[1].transform.GetChild(0).transform.GetChild(3).gameObject.SetActive(true);
            //Solar_eclipse.instance.LoadedModel[1].transform.GetChild(0).transform.GetChild(4).gameObject.SetActive(true);
            Solar_eclipse.instance.LoadedModel[1].transform.GetChild(2).gameObject.SetActive(true);
            Solar_eclipse.instance.LoadedModel[1].transform.GetChild(3).gameObject.SetActive(true);
        }

        LineRenderer [] lineRenderer =  GetComponentsInChildren<LineRenderer>();

        foreach (LineRenderer lineRender in lineRenderer)
        {
            Debug.Log("collider");
            lineRender.enabled = true;
        }

        Debug.Log(lineRenderer.Length);
        
        lineRenderer[0].SetPosition(0, Solar_eclipse.instance.LoadedModel[1].transform.GetChild(2).transform.GetChild(1).transform.position);
        lineRenderer[0].SetPosition(1, Solar_eclipse.instance.LoadedModel[4].transform.position);
        lineRenderer[1].SetPosition(0, Solar_eclipse.instance.LoadedModel[1].transform.GetChild(3).transform.GetChild(1).transform.position);
        lineRenderer[1].SetPosition(1, Solar_eclipse.instance.LoadedModel[5].transform.position);
        lineRenderer[2].SetPosition(0, Solar_eclipse.instance.LoadedModel[2].transform.position);
        lineRenderer[2].SetPosition(1, Solar_eclipse.instance.LoadedModel[4].transform.position);
        lineRenderer[3].SetPosition(0, Solar_eclipse.instance.LoadedModel[3].transform.position);
        lineRenderer[3].SetPosition(1, Solar_eclipse.instance.LoadedModel[4].transform.position);
        lineRenderer[4].SetPosition(0, Solar_eclipse.instance.LoadedModel[5].transform.position);
        lineRenderer[4].SetPosition(1, Solar_eclipse.instance.LoadedModel[2].transform.position);
        lineRenderer[5].SetPosition(0, Solar_eclipse.instance.LoadedModel[6].transform.position);
        lineRenderer[5].SetPosition(1, Solar_eclipse.instance.LoadedModel[3].transform.position);
    }
}
