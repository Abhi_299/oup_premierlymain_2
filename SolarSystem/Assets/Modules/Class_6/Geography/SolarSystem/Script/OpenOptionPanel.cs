﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OpenOptionPanel : MonoBehaviour
{
	public static float RotationSpeed;
	public GameObject optionPanel;
	public Scrollbar timeControl;
	public Button btnOpen;
	public Sprite sprtOpen;
	public Sprite sprtClose;

	public GameObject pnl_intro;
	public GameObject pnl_help;
	public GameObject pnl_Info;

	public GameObject infoHeading;
	public GameObject infoDes;

	public AudioClip buttonClick;

	public GameObject wizarPanel,wizarLogo,searchPanel;

	public enum WHAT_IS_SELECTED
	{
		SLIDER,
		HELP,
		INTRO,
		INFO}

	;

	bool isSliderOpen;
	bool isHelpOpen;
	bool isIntroOpen;
	bool isInfoOpen;

	Transform currObject;

	// Use this for initialization
	void Start ()
	{
	
	}

	void OnEnable(){
		LoadModelOnDetection.isModelLoaded += Model_Loading;
	}

	void OnDisable(){
		LoadModelOnDetection.isModelLoaded -= Model_Loading;
	}
	
	// Update is called once per frame
	void Update ()
	{		
		RotationSpeed = 1.2f - timeControl.value;
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began && !isInfoOpen && !isIntroOpen && !isHelpOpen) { 
				Ray ray = Camera.main.ScreenPointToRay (touch.position);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
					_openInfo (hit.transform.gameObject.name.ToString ());
					openInfo ();
				}
			}
		}
	}

	public void Model_Loading(string name,Transform _transform,bool status,LoadModelOnDetection.SceneType sceneType) {
		StartCoroutine (Model_Detection(_transform,status));
	}

	IEnumerator Model_Detection(Transform _transform,bool status){

		searchPanel.SetActive (!status);
		if (!status) {
			SolarSystem_ZoomInOut.instance.isModlDetected = false;
		}
		yield return new WaitForEndOfFrame ();
		if (status) {
			currObject = _transform.GetChild (0);
			SolarSystem_ZoomInOut.instance.currModel = currObject.gameObject;
			SolarSystem_ZoomInOut.instance.isModlDetected = true;
		}
	}
		
	public void openInfo ()
	{
		isInfoOpen = !isInfoOpen;
		if (isInfoOpen) {
			pnl_Info.SetActive (true);
		} else {
			pnl_Info.SetActive (false);
		}
		disableOther (WHAT_IS_SELECTED.INFO);
	}

	public void openSlider ()
	{
		isSliderOpen = !isSliderOpen;
		if (isSliderOpen) {
			btnOpen.GetComponent<Image> ().sprite = sprtClose;
			optionPanel.GetComponent<RectTransform> ().pivot = new Vector2 (1, 0.5f);
		} else {
			btnOpen.GetComponent<Image> ().sprite = sprtOpen;
			optionPanel.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0.5f);
		}
		disableOther (WHAT_IS_SELECTED.SLIDER);
	}



	public void openHelp ()
	{
		isHelpOpen = !isHelpOpen;
		if (isHelpOpen) {
			pnl_help.SetActive (true);
		} else {
			pnl_help.SetActive (false);
		}
		disableOther (WHAT_IS_SELECTED.HELP);
	}

	public void openIntro ()
	{
		isIntroOpen = !isIntroOpen;
		if (isIntroOpen) {
			pnl_intro.SetActive (true);
		} else {
			pnl_intro.SetActive (false);
		}
		disableOther (WHAT_IS_SELECTED.INTRO);
	}

	// To diable others buttons
	void disableOther (WHAT_IS_SELECTED arg)
	{
		playbuttonClick ();
		if (arg == WHAT_IS_SELECTED.SLIDER) {
			isHelpOpen = false;
			isIntroOpen = false;
			isInfoOpen = false;
			pnl_help.SetActive (false);
			pnl_intro.SetActive (false);
			pnl_Info.SetActive (false);
		} else if (arg == WHAT_IS_SELECTED.HELP) {
			isSliderOpen = false;
			isIntroOpen = false;
			isInfoOpen = false;
			optionPanel.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0.5f);
			pnl_intro.SetActive (false);
			pnl_Info.SetActive (false);
		} else if (arg == WHAT_IS_SELECTED.INTRO) {
			isSliderOpen = false;
			isHelpOpen = false;
			isInfoOpen = false;
			optionPanel.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0.5f);
			pnl_help.SetActive (false);
			pnl_Info.SetActive (false);
		} else if (arg == WHAT_IS_SELECTED.INFO) {
			isSliderOpen = false;
			isHelpOpen = false;
			isIntroOpen = false;
			optionPanel.GetComponent<RectTransform> ().pivot = new Vector2 (0, 0.5f);
			pnl_help.SetActive (false);
			pnl_intro.SetActive (false);
		}
	}

	void _openInfo (string arg)
	{
		if (arg == SolarSystemInfo.info [0, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [0, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [0, 0];
		} else if (arg == SolarSystemInfo.info [1, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [1, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [1, 0];
		} else if (arg == SolarSystemInfo.info [2, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [2, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [2, 0];
		} else if (arg == SolarSystemInfo.info [3, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [3, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [3, 0];
		} else if (arg == SolarSystemInfo.info [4, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [4, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [4, 0];
		} else if (arg == SolarSystemInfo.info [5, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [5, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [5, 0];
		} else if (arg == SolarSystemInfo.info [6, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [6, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [6, 0];
		} else if (arg == SolarSystemInfo.info [7, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [7, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [7, 0];
		} else if (arg == SolarSystemInfo.info [8, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [8, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [8, 0];
		} else if (arg == SolarSystemInfo.info [9, 0]) {
			infoDes.GetComponent<Text> ().text = "" + SolarSystemInfo.info [9, 1];
			infoHeading.GetComponent<Text> ().text = "" + SolarSystemInfo.info [9, 0];
		}
	}
	// Click sound
	public void playbuttonClick ()
	{
		GetComponent<AudioSource> ().PlayOneShot (buttonClick);
	}

	public void ShowSolarSystemLabels(){
		if (currObject != null) {
			playbuttonClick ();
			currObject.GetComponent<SolarSystemSceneController> ().show ();
		}
	}

	public void Click_CaptureBtn(){
		StartCoroutine (ManageUi());
	}

	IEnumerator ManageUi(){
		wizarPanel.SetActive (false);
		wizarLogo.SetActive (true);
		yield return new WaitForSeconds (3.5f);
		wizarPanel.SetActive (true);
		wizarLogo.SetActive (false);
	}
}
