﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SolarSystemSceneController : MonoBehaviour
{

	public GameObject[] orbit;
	public GameObject[] labels;
	public GameObject[] axis;
	public GameObject[] arrow;

	public GameObject PlanetParent;

	bool isShow;
	// Use this for initialization
	void Start ()
	{
		QualitySettings.antiAliasing = 4;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		for (int i = 0; i < orbit.Length; i++) {
			orbit [i].SetActive (true);
		}

		for (int i = 0; i < labels.Length; i++) {
			labels [i].SetActive (true);
		}

		for (int i = 0; i < axis.Length; i++) {
			axis [i].SetActive (true);
		}
		for (int i = 0; i < arrow.Length; i++) {
			arrow [i].SetActive (true);
		}

		isShow = true;

		Debug.Log ("Mercury active------- "+PlanetParent.activeInHierarchy);
		Debug.Log ("Mercury Position- "+PlanetParent.transform.position);
		Debug.Log ("Mercury Rotation- "+PlanetParent.transform.rotation);
		Debug.Log ("Mercury Scale- "+PlanetParent.transform.localScale);
	}


	public void show ()
	{
//		GetComponent<OpenOptionPanel> ().playbuttonClick ();
		isShow = !isShow;
		if (isShow) {
			for (int i = 0; i < orbit.Length; i++) {
				orbit [i].SetActive (true);
			}

			for (int i = 0; i < labels.Length; i++) {
				labels [i].SetActive (true);
			}

			for (int i = 0; i < axis.Length; i++) {
				axis [i].SetActive (true);
			}

			for (int i = 0; i < arrow.Length; i++) {
				arrow [i].SetActive (true);
			}
		} else {
			for (int i = 0; i < orbit.Length; i++) {
				orbit [i].SetActive (false);
			}

			for (int i = 0; i < labels.Length; i++) {
				labels [i].SetActive (false);
			}

			for (int i = 0; i < axis.Length; i++) {
				axis [i].SetActive (false);
			}

			for (int i = 0; i < arrow.Length; i++) {
				arrow [i].SetActive (false);
			}
		}
	}
}
