﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinksAndNames
{

	public static string assetBundleDynamicUrl = "";

	public static AssetBundleManifest manifestAssetBundle;

	public static string MenuJsonHeader = "Content";

	public static string pathToMenuJsonLocal = "/_Reusable/Json/";

	public static string pathToMenuIconsServer = "gs://wizarkids.appspot.com/Icons";

	public static string pathToMenuIconLocal = "/_Icons";

	public static string pathToFirebaseJson = "https://wizar-textbook.firebaseio.com/";

	public static string pathToLocalActivatedKeys = "/Resources/_Reusable/Json/";

	public static string nameOFActivationKeyJsonFile = "json_ActivatedKeys";

	public static string nameOfBookVersionJsonFile = "json_booksVersion";

	public static string activationKeyJsonHeader = "AuthKeys";

	public static string error01 = "Activation Not Allowed. Key has been used on maximum devices.";

	public static string error02 = "Could not connect to server, please check your internet connection.";

	public static string error03 = "Server is under maintenance, please be paitent and try after some time.";

	public static string error04 = "Wrong Activation Key. please check and re-enter the key.";

	public static string error05 = "This Activation key is not for the book you are tring to activate. Thankyou.";

	#if UNITY_ANDROID
	public static string PathToScreenshots = "/mnt/sdcard/DCIM/WizAR/";
	public static string PathToScreenshotsThumbnail = "/mnt/sdcard/DCIM/WizAR/";
	#endif

	#if UNITY_IOS
	public static string PathToScreenshots = Application.persistentDataPath + "/WizAR/";
	public static string PathToScreenshotsThumbnail = Application.persistentDataPath + "/WizAR/";
	#endif

}
