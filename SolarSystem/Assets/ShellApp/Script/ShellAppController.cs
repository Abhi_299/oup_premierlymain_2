﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShellAppController : MonoBehaviour
{
	public GameObject playButton;
	public GameObject LabelHead;
	public GameObject pnlAbtUs;
	public AudioClip clip;
	private bool loadScene = false;
	[SerializeField]
	private Text loadingText;

	bool isDetecting;
	bool isAboutUsOpen;
	// Use this for initialization
	void Start ()
	{
		loadingText.text = "No Image Target Detected";
		Invoke ("enablePlay", 0.5f);
	}

	void enablePlay ()
	{
		iTween.ScaleTo (playButton, new Vector3 (1, 1, 1), 0.5f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		// If the new scene has started loading...
		if (loadScene == true) {
			loadingText.transform.position = new Vector3 (loadingText.transform.position.x, loadingText.transform.position.y + 0.5f * Mathf.Sin (3 * Time.time), loadingText.transform.position.z);
		}
	}

	public void startDetecting ()
	{
		ShellAppDetectImageTarget.onImageTargetDetected += onImageTargetDetected;
		playButton.SetActive (false);
		LabelHead.SetActive (true);
	}


	void onImageTargetDetected (string arg)
	{
		for (int i = 0; i < ShellAppModuleList.MODULE_LIST.Length / 2; i++) {
			if (arg == ShellAppModuleList.MODULE_LIST [i, 0]) {
				loadingText.text = "Wait, Loading " + ShellAppModuleList.MODULE_LIST [i, 1];
				if (loadScene == false) {
					// ...set the loadScene boolean to true to prevent loading a new scene more than once...
					loadScene = true;
					// ...and start a coroutine that will load the desired scene.
					StartCoroutine (LoadNewScene (ShellAppModuleList.MODULE_LIST [i, 0]));
				}
			}
		}
	}

	// The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
	IEnumerator LoadNewScene (string arg)
	{

		// This line waits for 3 seconds before executing the next line in the coroutine.
		// This line is only necessary for this demo. The scenes are so simple that they load too fast to read the "Loading..." text.
		yield return new WaitForSeconds (1.5f);
		Debug.Log ("scene name " + arg);
		// Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
		AsyncOperation async = Application.LoadLevelAsync ("" + arg);

		// While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
		while (!async.isDone) {
			yield return null;
		}

	}


	void OnEnable ()
	{
		
	}


	void OnDisable ()
	{
		ShellAppDetectImageTarget.onImageTargetDetected -= onImageTargetDetected;
	}

	public void call ()
	{
		Application.OpenURL ("tel://+91-9540308134");
		playSound ();
	}

	public void SendEmail ()
	{

		string email = "contact@wizar.tech";

		string subject = MyEscapeURL ("quotation Needed");

		string body = MyEscapeURL ("Hi, I am interested in your product. Please set up a call where we can discuss more.");


		Application.OpenURL ("mailto:" + email + "?subject=" + subject + "&body=" + body);
		playSound ();

	}

	string MyEscapeURL (string url)
	{

		return WWW.EscapeURL (url).Replace ("+", "%20");

	}

	public void visitWebsite ()
	{
		Application.OpenURL ("http://wizar.tech/");
		playSound ();
	}

	public void openAboutUs ()
	{
		isAboutUsOpen = !isAboutUsOpen;
		if (isAboutUsOpen) {
			iTween.ScaleTo (pnlAbtUs, new Vector3 (1, 1, 1), 0.5f);
			iTween.ScaleTo (playButton, new Vector3 (0, 0, 0), 0.5f);
		} else {
			iTween.ScaleTo (pnlAbtUs, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (playButton, new Vector3 (1, 1, 1), 0.5f);
		}
		playSound ();
	}

	void playSound ()
	{
		GetComponent<AudioSource> ().PlayOneShot (clip);
	}

	public void closeAll ()
	{
		isAboutUsOpen = false;
		iTween.ScaleTo (pnlAbtUs, new Vector3 (0, 0, 0), 0.5f);
		iTween.ScaleTo (playButton, new Vector3 (1, 1, 1), 0.5f);
	}

	public void closeApp ()
	{
		Application.Quit ();
	}
}
