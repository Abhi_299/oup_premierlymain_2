
using LitJson;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class FirebaseDataHandler : MonoBehaviour {

	// actKeyHeader = json's main key name, currentActKey = activation key entered by user, currentIEMI = iemi of the device used by user for authentication purpose
	private string actKeyHeader,currentActKey,currentIEMI;	

	// devicesLeft = when user enters Activation key and request server for authentication, user recieves a response which confirms how many more devices can be registered.
	private int devicesLeft = 0;

	// firebaseUrl = link to firebase server, pathToActBooks = internal path where activated books info is kept, nameOfActBookJsonFile = name of the json file which contains all Activated Books information
	private string firebaseUrl,pathToActBooks, nameOfActBookJsonFile;

	// bookName = name of the book fetched from server via entered Activation key, currentBookName = name of the book user is trying to activate.
	private string bookName,currentBookName;

	// isTransactionFirstTime = is just a patch check that device registration on server happens once per activation process. 
	private int isTransactionFirstTime = 0;

	// activationKeyData = json data which is dynamically fetched from server using firebaseUrl and Activation key
	private JsonData activationKeyData; 

	// app = The entry point of Firebase SDKs. It holds common configuration and state for Firebase APIs.
	FirebaseApp app;

	// An enum that indicates the current status of the dependencies on the current system, required for Firebase to run.
	DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;

	// making this class a singolton class to be used from any other class.
	public static FirebaseDataHandler firebaseDataHandler;

	// isActivated = response after all the verifications of Activation key, this boolean will be used in diffrent places for knowing that Activation was done successfully
	public bool isActivated = false;

	// AnimCanvas2D = to trigger animation from one another of Canvas UI.
	public GameObject AnimCanvas2D,GO_Message,GO_ActivationKey;

	// AlertText = will dynamically fetch error message from LinksAndNames class according to diffrent errors occured. 
	public Text AlertText;

	void Start() {
		firebaseDataHandler = this;
		isTransactionFirstTime = 0;
		pathToActBooks = LinksAndNames.pathToLocalActivatedKeys;
		nameOfActBookJsonFile = LinksAndNames.nameOFActivationKeyJsonFile;
		actKeyHeader = LinksAndNames.activationKeyJsonHeader;
	}

	public void InitiateActivationKeyCheck(string UserEnteredKey,string bookNameFromContent){
		isTransactionFirstTime = 0;
		currentBookName = bookNameFromContent;
		currentActKey = UserEnteredKey;
		currentIEMI = SystemInfo.deviceUniqueIdentifier;
		firebaseUrl = LinksAndNames.pathToFirebaseJson;
		pathToActBooks = LinksAndNames.pathToLocalActivatedKeys;
		nameOfActBookJsonFile = LinksAndNames.nameOFActivationKeyJsonFile;
		actKeyHeader = LinksAndNames.activationKeyJsonHeader;
		dependencyStatus = FirebaseApp.CheckDependencies();
		if (dependencyStatus != DependencyStatus.Available) {
			FirebaseApp.FixDependenciesAsync().ContinueWith(task => {
				dependencyStatus = FirebaseApp.CheckDependencies();
				if (dependencyStatus == DependencyStatus.Available) {
					InitializeFirebase();
				} else {
					AlertText.text = LinksAndNames.error02;
					AnimCanvas2D.SetActive(true);
				}
			});
		} else {
			InitializeFirebase();
		}
	}

	// Initialize the Firebase database:
	void InitializeFirebase() {
		app = FirebaseApp.DefaultInstance;
		app.SetEditorDatabaseUrl(firebaseUrl);
		StartCoroutine(checkInternetConnection((isConnected)=>{
			if(isConnected){
					FirebaseDatabase.DefaultInstance
						.GetReference(actKeyHeader+"/"+currentActKey)
						.ValueChanged += (object sender2, ValueChangedEventArgs e2) => {
					if (e2.DatabaseError != null) {																		// Instances of DatabaseError are passed to callbacks when an operation failed. They contain a description of the specific error that occurred.
						AlertText.text =  LinksAndNames.error02;														// If We recieved DatabaseError, we are prompting a popup saying try again after some time.
						AnimCanvas2D.SetActive(true);																	// Running Animation from ActivationKey_Authenticating to Error_AK
						return;
						}

						if(e2.Snapshot.GetRawJsonValue()!=null){
						activationKeyData = JsonMapper.ToObject (e2.Snapshot.GetRawJsonValue());						// Fetch sub-json from firebaseUrl+currentActKey and save it in as Json Data
						bookName = activationKeyData ["bookName"].ToString ();
						try{
									devicesLeft = Convert.ToInt32(activationKeyData ["left"].ToString());				// Know that is space left for registering new device
									if(devicesLeft>0 && isTransactionFirstTime == 0){									
										isTransactionFirstTime = 1;
										Adddevice();
							}else if (devicesLeft==0 && isTransactionFirstTime == 0){
										isTransactionFirstTime = 1;
										CompareBooks();
										return;
									}
							}catch(Exception ee){
							Debug.Log (ee.ToString());
							AlertText.text =  LinksAndNames.error03;
							AnimCanvas2D.SetActive(true);
									ReportError("'left' of Activation key '"+currentActKey+"' was not found");
									return;
							}
					}else{
						AlertText.text =  LinksAndNames.error04;
						AnimCanvas2D.SetActive(true);
							return;
						}
					};
			}else{
				AlertText.text =  LinksAndNames.error02;
				AnimCanvas2D.SetActive(true);
				return;
			}
		}));
	}
		
	void Adddevice() {
		// MaximumDeviceSupported = will hold the value of maximum supported devices for perticular Activation key, deviceNumber = max - left devices i.e. the position or index were new mobile will be registered.
		int MaximumDeviceSupported, deviceNumber;

		// A Reference represents a specific location in your Database and can be used for reading or writing data to that Database location.
		DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference(actKeyHeader+"/"+currentActKey);

		if (reference == null) {
			AlertText.text =  LinksAndNames.error02;
			AnimCanvas2D.SetActive(true);
			return;
		}

		try{
			MaximumDeviceSupported = Convert.ToInt32 (activationKeyData ["max"].ToString ());
		}catch(Exception ee){
			Debug.Log (ee.ToString());
			AlertText.text =  LinksAndNames.error03;
			AnimCanvas2D.SetActive(true);
			ReportError("'max' of Activation key '"+currentActKey+"' was not found");
			return;
		}
		try{
			if(currentBookName != bookName){
				AlertText.text =  LinksAndNames.error05;
				AnimCanvas2D.SetActive(true);
				return;
			}
		}catch(Exception ee){
			Debug.Log (ee.ToString());
			AlertText.text =  LinksAndNames.error03;
			AnimCanvas2D.SetActive(true);
			ReportError("'bookName' of Activation key '"+currentActKey+"' was not found");
			return;
		}

		deviceNumber = MaximumDeviceSupported - devicesLeft;
		string[] deviceUniqueIDFormServer = new string[deviceNumber+1];																			// max - left = current array index i.e. 0,1,2,3 and so on. So size of string array will be +1

		for (int i = 0; i < deviceUniqueIDFormServer.Length; i++) {
			if (i + 1 == deviceUniqueIDFormServer.Length) {
				try{
				deviceUniqueIDFormServer [i] = currentIEMI;
				}catch(Exception ee){
					Debug.Log (ee.ToString());
					AlertText.text =  LinksAndNames.error03;
					AnimCanvas2D.SetActive(true);
					ReportError("Unique Id was not detected for '"+Application.platform+"' device.");
					return;
				}
			} else {
				try{
					if(activationKeyData ["devices"] [i].ToString () == currentIEMI){
						SaveActivatedBook();
						return;
					}
				}catch(Exception ee){
					Debug.Log (ee.ToString());
					AlertText.text =  LinksAndNames.error03;
					AnimCanvas2D.SetActive(true);
						ReportError("'devices"+i+"' of Activation key '"+currentActKey+"' was not found");
						return;
				}
				deviceUniqueIDFormServer [i] = activationKeyData ["devices"] [i].ToString ();
			}
		}

		UpdateServerSideValues updateJson = new UpdateServerSideValues(deviceUniqueIDFormServer,devicesLeft,MaximumDeviceSupported,bookName);

		string json = JsonUtility.ToJson(updateJson);
		reference.SetRawJsonValueAsync(json);
		SaveActivatedBook ();
	}

	void CompareBooks(){
		try {
				for (int i = 0; i < activationKeyData ["devices"].Count; i++) {
					if (activationKeyData ["devices"] [i].ToString () == currentIEMI) {
						SaveActivatedBook ();
						return;
				}}
				AlertText.text = LinksAndNames.error01;
				AnimCanvas2D.SetActive(true);
			} catch (Exception ee) {
				Debug.Log (ee.ToString ());
				AlertText.text = LinksAndNames.error03;
				AnimCanvas2D.SetActive(true);
				ReportError ("'devices of Activation key '" + currentActKey + "' was not found");
  		}
	}

	void ReportError(string message){
			DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("/Errors");
		if (reference == null) {
			AlertText.text =  LinksAndNames.error03;
				return;
			}
			message = message + " : "+System.DateTime.Now;
			ReportToServerSide updateJson = new ReportToServerSide(message);
			string json = JsonUtility.ToJson(updateJson);
			reference.Child(currentIEMI).SetRawJsonValueAsync(json);
	}

	IEnumerator checkInternetConnection(Action<bool> action){
		WWW www = new WWW("http://google.com");
		yield return www;
		if (www.error != null) {
			action (false);
		} else {
			action (true);
		}
	} 

	public bool isBookAvialable(string currentBookName){													// Check if book is already Activated and Downloaded.
		string jsonText = "",activationKeyPath ="";	
		JsonData jsonData = new JsonData ();
		int totalBooks = 0;
		activationKeyPath = Application.persistentDataPath + pathToActBooks + nameOfActBookJsonFile + ".json";

		if (System.IO.File.Exists (activationKeyPath)) {
			jsonText = System.IO.File.ReadAllText (activationKeyPath);	
			jsonData = JsonMapper.ToObject (jsonText);
			totalBooks = Convert.ToInt32 (jsonData ["keys"].ToString ());	
			for (int i = 0; i < totalBooks; i++) {
				if (jsonData ["books"] [i].ToString () == currentBookName) {
					return true;
				}
			}
		} else {
			return false;
		}
		return false;
	}

	void SaveActivatedBook()																										// If Key is correct, we will save the key and activate the current book, so that user don't have to activate the book again and again.
	{
		string jsonText = "",activationKeyPath ="";	
		JsonData jsonData = new JsonData ();
		int totalBooks = 0;
		string[] books;																
		activationKeyPath = Application.persistentDataPath + pathToActBooks + nameOfActBookJsonFile + ".json";

		if (System.IO.File.Exists (activationKeyPath)) {
			jsonText = System.IO.File.ReadAllText (activationKeyPath);	
			jsonData = JsonMapper.ToObject (jsonText);
			totalBooks = Convert.ToInt32 (jsonData ["keys"].ToString ()) + 1;															// +1 for adding new bookName
			books = new string[totalBooks];
			for(int i=0;i<totalBooks;i++){
				if (i == totalBooks - 1)	
					books [i] = bookName;
				else books [i] = jsonData ["books"][i].ToString ();
			}
		}  else {
			books = new string[1];
			books [0] = bookName;
		}

		SaveActivationKeyJson jsonObject = new SaveActivationKeyJson(books,books.Length);
		jsonText = JsonUtility.ToJson (jsonObject);
		if(!Directory.Exists(Application.persistentDataPath + pathToActBooks))
			Directory.CreateDirectory(Application.persistentDataPath + pathToActBooks);
		System.IO.File.WriteAllText (activationKeyPath, jsonText);   										// To write my Json file for future use
		isActivated = true;
	}

	public void button_ClosePressed(){
		GO_Message.SetActive(false);
		AnimCanvas2D.SetActive(false);
		GO_ActivationKey.SetActive(true);
	}

}


public class UpdateServerSideValues
{
	public string[] devices;
	public int left;
	public int max;
	public string bookName;

	public UpdateServerSideValues(string[] iemi,int devicesLeft,int maximumDevicesSupported,string book){
		devices = iemi;
		left = devicesLeft-1;
		max = maximumDevicesSupported;
		bookName = book;
	}
}


public class ReportToServerSide
{
	public string message;

	public ReportToServerSide(string errorMessage){
		message = errorMessage;
	}
}


public class SaveActivationKeyJson
{
	public string[] books;
	public int keys;

	public SaveActivationKeyJson(string[] bookNames, int totalKeys){
		books = bookNames;
		keys = totalKeys;
	}
}