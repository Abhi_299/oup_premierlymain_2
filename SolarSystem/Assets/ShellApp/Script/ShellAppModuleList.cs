﻿using UnityEngine;
using System.Collections;

public class ShellAppModuleList : MonoBehaviour
{
	public static string[,] MODULE_LIST = new string[,] {
		{ "InteriorOfEarth", "Interior Of Earth" },
		{ "solid_state_packings", "Solid State Packings" },
		{ "conformations_of_cyclohexane", "Conformations Of Cyclohexane" },
		{ "preposition", "Prepositions" },
		{ "SolarSystem", "Solar System" },
		{ "SeasonOfEarth", "Seasons Of Earth" },
		{ "RespiratorySystem", "Respiratory System" },
		{ "PlantCell", "Plant Cell" },
		{ "PartsOfBody", "Parts Of Body" },
		{ "DigestiveSystem", "Digestive System" },
		{ "AnimalCell", "Animal Cell" },
		{ "mensuration", "Mensuration" },
		{ "projectile_motion", "Projectile Motion" },
		{ "WaterCycle", "Water Cycle" }
	};
}
