﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShellAppLogo : MonoBehaviour {

	public GameObject GO_ActivationKey,go_Message,logo,logoCenter;
	public Text TextActivationKey;
	bool isMenuCalled;
	// Use this for initialization
	void Start () {
		isMenuCalled = false;
		GO_ActivationKey.SetActive (false);
		go_Message.SetActive (false);
		logo.SetActive (false);
		logoCenter.SetActive(true);
		StartCoroutine ("StartfetchingBook");
	}

	IEnumerator StartfetchingBook ()
	{
		bool checkBookStatus = FirebaseDataHandler.firebaseDataHandler.isBookAvialable ("Menu");
		yield return checkBookStatus;
		if (!checkBookStatus) {
			logo.SetActive (true);
			logoCenter.SetActive(false);
			GO_ActivationKey.SetActive (true);
		} else {
			SceneManager.LoadScene ("Menu");
		}
	}


	void Update(){
		if(FirebaseDataHandler.firebaseDataHandler.isActivated){
			if (!isMenuCalled) {
				isMenuCalled = true;
				SceneManager.LoadScene ("Menu");
			}
		}
	}

	public void button_OKPressed(){
		go_Message.SetActive (true);
		GO_ActivationKey.SetActive (false);
		FirebaseDataHandler.firebaseDataHandler.InitiateActivationKeyCheck (TextActivationKey.text, "Menu");
	}
}
