﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System.Collections.Generic;
using System;


public class VuforiaDataSetController : MonoBehaviour
{
	// specify these in Unity Inspector
	public enum DataSetNames
	{
		class_02_evs_parts_of_body,
		class_03_english_preposition,
		class_06_geography_seaons_of_earth,
		class_06_science_biology_digestive_system,
		class_06_science_biology_respiratory_system,
		class_06_geography_solar_system,
		class_07_science_chemistry_water_cycle,
		class_07_science_geography_interior_of_earth,
		class_08_science_biology_animal_cell,
		class_08_science_biology_plant_cell,
		class_09_science_chemistry_solid_state,
		class_10_science_chemistry_cyclohexane,
		class_10_science_math_mensuration,
		class_11_science_physics_projectile}

	;

	public DataSetNames DataSetEnums;
	string dataSetName = "";
	//  Assets/StreamingAssets/QCAR/DataSetName

	// Use this for initialization
	void Start ()
	{
		// Vuforia 6.2+
		dataSetName = Enum.GetName (typeof(DataSetNames), DataSetEnums); 
		Debug.Log (dataSetName);
		VuforiaARController.Instance.RegisterVuforiaStartedCallback (LoadDataSet);
	}

	void LoadDataSet ()
	{

		ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker> ();

		DataSet dataSet = objectTracker.CreateDataSet ();

		if (dataSet.Load (dataSetName)) {
			objectTracker.Stop ();  // stop tracker so that we can add new dataset
			if (!objectTracker.ActivateDataSet (dataSet)) {
				Debug.Log ("<color=yellow>Failed to Activate DataSet: " + dataSetName + "</color>");
			}
			if (!objectTracker.Start ()) {
				Debug.Log ("<color=yellow>Tracker Failed to Start.</color>");
			}
		} else {
			Debug.LogError ("<color=yellow>Failed to load dataset: '" + dataSetName + "'</color>");
		}
	}

	void OnDisable ()
	{
		VuforiaARController.Instance.UnregisterVuforiaStartedCallback (LoadDataSet);
	}
}
