﻿using UnityEngine;
using System.Collections;

public class WC_LightningRandom : MonoBehaviour {
	LineRenderer lineRend;
	bool shuted;
	public float maxDisabledTime,maxEnabledTime;
	// Use this for initialization
	void Start () {
		lineRend = this.GetComponent<LineRenderer>();
		lineRend.enabled = shuted;
		InvokeRepeating ("random",maxDisabledTime,maxDisabledTime);
	}

	void random(){
		if(WaterCycleController.isTargetFound)shuted = true;
		Invoke ("shut",maxEnabledTime);
	}

	void Update(){
		if (this.gameObject.activeInHierarchy) {
			lineRend.enabled = shuted;
			if(!this.GetComponent<AudioSource> ().isPlaying && lineRend.isVisible){
				this.GetComponent<AudioSource> ().Play ();

			}
		}
		if (!WaterCycleController.isTargetFound) {
			shuted = false;
			lineRend.enabled = shuted;
		}
	}
	void shut(){
		shuted = false;
	}
}
