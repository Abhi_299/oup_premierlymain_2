﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// While renaming the scipts refactor all the classes names with a prefix of ProjectName_
using System;


public class WaterCycleController : MonoBehaviour
{
	
	// parent of info , intro, help canvas to disable when image target is lost
	public GameObject parentCanvas;

	public GameObject pnl_intro;
	public GameObject pnl_help;
	public GameObject pnl_Info;

	public GameObject infoHeading;
	public GameObject infoDes;

	int daycount;
	// Panle when image target is lost
	public GameObject pnl_NoImageTarget;

	public AudioClip buttonClick;

	// current focus
	public enum WHAT_IS_SELECTED
	{
		HELP,
		INTRO,
		INFO,
		NO_IMAGE_TARGET}

	;

	// On/Off switch
	bool isInfoOpen;
	bool isHelpOpen;
	bool isIntroOpen;
	public Text definationText;
	public static int definationOf;
	public String[] _textData = {"EVAPORATION\n\n\n\n\nThe sun, which drives the water cycle, heats water in oceans and seas. Water evaporates as water vapor into the air. Ice and snow can sublimate directly into water vapour.Due to the significant difference in molecular mass, water vapor in gas form gain height in open air as a result of buoyancy.",
		"CONDENSATION\n\n\n\n\nThe lowered temperature causes water vapour to condense into a tiny liquid water droplet which is heavier than the air, such that it falls unless supported by an updraft. A huge concentration of these droplets over a large space up in the atmosphere become visible as cloud.",
		"PRECIPITATION\n\n\n\n\nAir currents move water vapour around the globe, cloud particles collide, grow, and fall out of the upper atmospheric layers as precipitation. Some precipitation falls as snow or hail, sleet, and can accumulate as ice caps and glaciers, which can store frozen water for thousands of years.",
		"COLLECTION\n\n\n\n\nIn river valleys and floodplains there is often continuous water exchange between surface water and groundwater in the hyporheic zone. Over time, the water returns to the ocean, to continue the water cycle.",
	};
	public static bool isTargetFound;
	public bool is3DModule;
	// variable used for only 3d scene

	void Start ()
	{
		if (is3DModule)
			ThreeD_SceneCalled ();
	}

	// When i is clicked on the main screen to show what is app about
	public void opnIntro ()
	{
		playbuttonClick ();
		isIntroOpen = !isIntroOpen;
		if (isIntroOpen) {
			iTween.ScaleTo (pnl_intro, new Vector3 (1, 1, 1), 0.5f);
		} else {
			iTween.ScaleTo (pnl_intro, new Vector3 (0, 0, 0), 0.5f);
		}
		disableOther (WHAT_IS_SELECTED.INTRO);
	}



	// When ? is clicked on the main screen to show how to use this app
	public void openHelp ()
	{
		playbuttonClick ();
		isHelpOpen = !isHelpOpen;
		if (isHelpOpen) {
			iTween.ScaleTo (pnl_help, new Vector3 (1, 1, 1), 0.5f);
		} else {
			iTween.ScaleTo (pnl_help, new Vector3 (0, 0, 0), 0.5f);
		}
		disableOther (WHAT_IS_SELECTED.HELP);
	}

	// when a label is taped on the scene
	public void openInfo (string arg)
	{
		isInfoOpen = !isInfoOpen;
		disableOther (WHAT_IS_SELECTED.INFO);
		Debug.Log ("arg on click " + arg);
		if (arg != null) {
			for (int i = 0; i < WC_Info.info.Length / 2; i++) {
				Debug.Log ("arg trim " + arg.Trim () + "  info value " + WC_Info.info [i, 0]);
				if (arg.Trim () == WC_Info.info [i, 0]) {
                    playbuttonClick();
                    infoHeading.GetComponent<Text> ().text = "" + WC_Info.info [i, 0];
					infoDes.GetComponent<Text> ().text = "" + WC_Info.info [i, 1];
					iTween.ScaleTo (pnl_Info, new Vector3 (1, 1, 1), 0.5f);
					return; 
				} else {
					iTween.ScaleTo (pnl_Info, new Vector3 (0, 0, 0), 0.5f);
				}
			}
		}
	}

	// To diable others panels when selected
	void disableOther (WHAT_IS_SELECTED arg)
	{
		if (arg == WHAT_IS_SELECTED.HELP) {
			isIntroOpen = false;
			isInfoOpen = false;
			iTween.ScaleTo (pnl_intro, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (pnl_Info, new Vector3 (0, 0, 0), 0.5f);
		} else if (arg == WHAT_IS_SELECTED.INTRO) {
			isHelpOpen = false;
			isInfoOpen = false;

			iTween.ScaleTo (pnl_help, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (pnl_Info, new Vector3 (0, 0, 0), 0.5f);
		} else if (arg == WHAT_IS_SELECTED.INFO) {
			isHelpOpen = false;
			isIntroOpen = false;

			iTween.ScaleTo (pnl_help, new Vector3 (0, 0, 0), 0.5f);
			iTween.ScaleTo (pnl_intro, new Vector3 (0, 0, 0), 0.5f);
		}
	}

	// Events trigger for Image Target detected added in the current reference
	// _ImageTargetDetected is in Reusable folder in the assets
	void OnEnable ()
	{
		LoadModelOnDetection .isModelLoaded += onImageTargetDetected;
	}

	// Events trigger Image Target detected removed in the current reference
	// _ImageTargetDetected is in Reusable folder in the assets
	void OnDisable ()
	{
		LoadModelOnDetection .isModelLoaded += onImageTargetDetected;
		if (pnl_NoImageTarget != null)
			iTween.ScaleTo (pnl_NoImageTarget, new Vector3 (1, 1, 1), 0.25f);
		if (parentCanvas != null)
			parentCanvas.SetActive (false);
		isTargetFound = false;
	}


	// Enable disable parent canvas on the scene when Image Target is found and/or lost
	void onImageTargetDetected (string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
	{
		if (status) {
			if (pnl_NoImageTarget != null)
				iTween.ScaleTo (pnl_NoImageTarget, new Vector3 (0, 0, 0), 0.25f);
			if (parentCanvas != null)
				parentCanvas.SetActive (true);

			
			isTargetFound = true;
		}
		if (!status) {
			if (pnl_NoImageTarget != null)
				iTween.ScaleTo (pnl_NoImageTarget, new Vector3 (1, 1, 1), 0.25f);
			if (parentCanvas != null)
				parentCanvas.SetActive (false);
			isTargetFound = false;
		}
		if (@_transform != null&& _transform.transform.childCount>0)
		{
			for (int i = 0; i < _transform.transform.childCount; i++)
			{
				Debug.Log("arg value "+status);
				_transform.transform.GetChild(i).gameObject.SetActive(status);
			}
		}
	}

	// Click sound
	void playbuttonClick ()
	{
		GetComponent<AudioSource> ().PlayOneShot (buttonClick);
	}

	// When i is clicked on the main screen to show what is app about
	public void opnDefination ()
	{
		playbuttonClick ();
		isInfoOpen = !isInfoOpen;

		if (definationOf >= 0 && definationOf < _textData.Length)
			definationText.text = _textData [definationOf];
		
		if (isInfoOpen) {
			iTween.ScaleTo (pnl_Info, new Vector3 (1, 1, 1), 0.5f);
		} else {
			iTween.ScaleTo (pnl_Info, new Vector3 (0, 0, 0), 0.5f);
		}
		disableOther (WHAT_IS_SELECTED.INFO);
	}

	// method used for only 3d scene
	void ThreeD_SceneCalled ()
	{
		if (pnl_NoImageTarget != null)
			iTween.ScaleTo (pnl_NoImageTarget, new Vector3 (0, 0, 0), 0.25f);
		if (parentCanvas != null)
			parentCanvas.SetActive (true);
		isTargetFound = true;
	}

}
