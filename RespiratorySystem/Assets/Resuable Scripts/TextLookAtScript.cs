﻿using UnityEngine;
using System.Collections;

public class TextLookAtScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 look = Camera.main.transform.position ;
//		look.z = 0;
		transform.rotation =  Quaternion.LookRotation(-look);
	}
}
