﻿using UnityEngine;
using System.Collections;

public class RespiratoryInfo : MonoBehaviour
{

	public static string[,] info = new string[,] { {
			"Nasal Passage",
			"Nasal passage is a channel for airflow through the nose. The walls of the nasal passages are coated with respiratory mucous membranes, which contain innumerable tiny hair-like cells that move waves of mucus toward the throat."
		}, {
			"Alveoli",
			"Alveoli are tiny sacs within our lungs that allow oxygen and carbon dioxide to move between the lungs and bloodstream. Learn more about how they function and quiz your knowledge at the end."
		}, {
			"Lungs",
			"The lungs are the primary organs of respiration in humans and many other animals including a few fish and some snails. In mammals and most other vertebrates, two lungs are located near the backbone on either side of the heart."
		}, {
			"Oral_Cavity",
			"The oral cavity includes the lips, the inside lining of the lips and cheeks (buccal mucosa), the teeth, the gums, the front two-thirds of the tongue, the floor of the mouth below the tongue, and the bony roof of the mouth (hard palate)."
		}, {
			"Pharynx",
			"The pharynx is the part of the throat that is behind the mouth and nasal cavity. These tubes going down to the stomach and the lungs."
		}, {
			"Trachea",
			"The trachea is colloquially called the windpipe. It is jointly originated with foodpipe from the mouth but separates from foodpipe through Epiglottis."
		}
	};
	public static string[] heading = new string[] {
		"Nasal Passage",
		"Alveoli",
		"Lungs",
		"Oral Cavity",
		"Pharynx",
		"Trachea"
	};
}
